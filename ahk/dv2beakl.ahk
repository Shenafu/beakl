; Balanced Effortless Advanced Keyboard Layout
; convert dvorak to the BEAKL layout

;UNSHIFTED
;,youf vdsc.
;hieak mtnrl
;'zjpx bgwq"

;SHIFTED
;(YOUF VDSC)
;HIEAK MTNRL
;;ZJPX BGWQ:

#SingleInstance force
#InstallKeybdHook
#UseHook on

;; Top row
'::Send, {,}
,::Send, y
.::Send, o
p::Send, u
y::Send, f
f::Send, v
g::Send, d
c::Send, s
r::Send, c
l::Send, .

+'::Send, (
+,::Send, Y
+.::Send, O
+p::Send, U
+y::Send, F
+f::Send, V
+g::Send, D
+c::Send, S
+r::Send, C
+l::Send, )

;; Home row
a::Send, h
o::Send, i
e::Send, e
u::Send, a
i::Send, k
d::Send, m
h::Send, t
t::Send, n
n::Send, r
s::Send, l

+a::Send, H
+o::Send, I
+e::Send, E
+u::Send, A
+i::Send, K
+d::Send, M
+h::Send, T
+t::Send, N
+n::Send, R
+s::Send, L

;; Bottom row
SC02C::Send, '
q::Send, z
j::Send, j
k::Send, p
x::Send, x
b::Send, b
m::Send, g
w::Send, w
v::Send, q
z::Send, "

+SC02C::Send, {;}
+q::Send, Z
+j::Send, J
+k::Send, P
+x::Send, X
+b::Send, B
+m::Send, G
+w::Send, W
+v::Send, Q
+z::Send, :

;; Number row
;`::Send, 7
;1::Send, 9
;2::Send, 5
;3::Send, 1
;4::Send, 3
;5::Send, =
;6::Send, /
;7::Send, 2
;8::Send, 0
;9::Send, 4
;0::Send, 6
;[::Send, 8
;]::Send, \

;+`::Send, {SC029} ; backquote
;+1::Send, +2
;+2::Send, +[
;+3::Send, +/
;+4::Send, +5
;+5::Send, {+}
;+6::Send, +8
;+7::Send, +7
;+8::Send, +1
+9::Send, <
+0::Send, >
;+[::Send, +6
;+]::Send, +4

#UseHook off

;
