; Balanced Effortless Advanced Keyboard Layout
; convert dvorak to the BEAKL4 layout

;UNSHIFTED
;qscg. ,uofj
;wnthm pieay
;"vlrk xdbz'

;SHIFTED
;QSCG( )UOFJ
;WNTHM PIEAY
;;VLRK XDBZ:

#SingleInstance force
#InstallKeybdHook
#UseHook on

;; Top row
'::Send, q
,::Send, c
.::Send, s
p::Send, g
y::Send, .
f::Send, ,
g::Send, u
c::Send, o
r::Send, f
l::Send, j

+'::Send, Q
+,::Send, C
+.::Send, S
+p::Send, G
+y::Send, {(}
+f::Send, {)}
+g::Send, U
+c::Send, O
+r::Send, F
+l::Send, {;}

;; Home row
a::Send, w
o::Send, n
e::Send, t
u::Send, h
i::Send, m
d::Send, p
h::Send, i
t::Send, e
n::Send, a
s::Send, y

+a::Send, W
+o::Send, N
+e::Send, T
+u::Send, H
+i::Send, M
+d::Send, P
+h::Send, I
+t::Send, E
+n::Send, A
+s::Send, Y

;; Bottom row
SC02C::Send, "
q::Send, v
j::Send, l
k::Send, r
x::Send, k
b::Send, x
m::Send, d
w::Send, b
v::Send, z
z::Send, '

+SC02C::Send, {;}
+q::Send, V
+j::Send, L
+k::Send, R
+x::Send, K
+b::Send, X
+m::Send, D
+w::Send, B
+v::Send, Z
+z::Send, :

;; Number row
;`::Send, 7
;1::Send, 9
;2::Send, 5
;3::Send, 1
;4::Send, 3
;5::Send, =
;6::Send, /
;7::Send, 2
;8::Send, 0
;9::Send, 4
;0::Send, 6
;[::Send, 8
;]::Send, \

;+`::Send, {SC029} ; backquote
;+1::Send, +2
;+2::Send, +[
;+3::Send, +/
;+4::Send, +5
;+5::Send, {+}
;+6::Send, +8
;+7::Send, +7
;+8::Send, +1
+9::Send, <
+0::Send, >
;+[::Send, +6
;+]::Send, +4

#UseHook off

;
