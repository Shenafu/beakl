; Balanced Effortless Advanced Keyboard Layout
; convert dvorak to the BEAKL Opted layout

;UNSHIFTED
;  40123 76598
;  qyoux gdnmz
;- ,hea. csrtw ;
;  jk/i' bplfv

;SHIFTED
;  QYOUX GDNMZ
;~ !HEA@ CSRTW |
;  JK?I` BPLFV

;ALTGR / SCROLL LOCK
;   +=*   ^%~
;   <$>   [_]
;- \(")# %{=}| ;
;   :*+   &^~

;NUM LOCK
;   +=*
; -523:
; 7.104
;  /698,

#SingleInstance force
;#InstallKeybdHook
#UseHook on


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Number row

1::Send, 4

+2::Send, {+}
2::
{
	if GetKeyState("CapsLock", "T")
		Send, {+ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {+ DownTemp}
	else
		Send, {0 DownTemp}
	Return
}
2 up::Send, {0 Up}


+3::Send, {=}
3::
{
	if GetKeyState("CapsLock", "T")
		Send, {= DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {= DownTemp}
	else
		Send, {1 DownTemp}
	Return
}
3 up::Send, {1 Up}


+4::Send, {*}
4::
{
	if GetKeyState("CapsLock", "T")
		Send, {* DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {* DownTemp}
	else
		Send, {2 DownTemp}
	Return
}
4 up::Send, {2 Up}


5::Send, 3
6::Send, 7


+7::Send, {^}
7::
{
	if GetKeyState("CapsLock", "T")
		Send, {^ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {^ DownTemp}
	else
		Send, {6 DownTemp}
	Return
}
7 up::Send, {6 Up}


+8::Send, {`%}
8::
{
	if GetKeyState("CapsLock", "T")
		Send, {`% DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {`% DownTemp}
	else
		Send, {5 DownTemp}
	Return
}
8 up::Send, {5 Up}


+9::Send, {~}
9::
{
	if GetKeyState("CapsLock", "T")
		Send, {~ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {~ DownTemp}
	else
		Send, {9 DownTemp}
	Return
}
9 up::Send, {9 Up}


0::Send, 8


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Top row

+'::Send, {Q DownTemp}
'::
{
	if GetKeyState("CapsLock", "T")
		Send, {- DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {- DownTemp}
	else
		Send, {q DownTemp}
	Return
}
' up::Send, {q Up}


+,::Send, {Y DownTemp}
,::
{
	if GetKeyState("CapsLock", "T")
		Send, {< DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {5 DownTemp}
	else
		Send, {y DownTemp}
	Return
}
, up::Send, {y Up}


+.::Send, {O DownTemp}
.::
{
	if GetKeyState("CapsLock", "T")
		Send, {$ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {2 DownTemp}
	else
		Send, {o DownTemp}
	Return
}
. up::Send, {o Up}


+p::Send, {U DownTemp}
p::
{
	if GetKeyState("CapsLock", "T")
		Send, {> DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {3 DownTemp}
	else
		Send, {u DownTemp}
	Return
}
p up::Send, {u Up}


+y::Send, {X DownTemp}
y::
{
	if GetKeyState("CapsLock", "T")
		Send, {X DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {: DownTemp}
	else
		Send, {x DownTemp}
	Return
}
y up::Send, {x Up}


f::g


+g::Send, {D DownTemp}
g::
{
	if GetKeyState("CapsLock", "T")
		Send, {[ DownTemp}
	else
		Send, {d DownTemp}
	Return
}
g up::Send, {d Up}


+c::Send, {N DownTemp}
c::
{
	if GetKeyState("CapsLock", "T")
		Send, {_ DownTemp}
	else
		Send, {n DownTemp}
	Return
}
c up::Send, {n Up}


+r::Send, {M DownTemp}
r::
{
	if GetKeyState("CapsLock", "T")
		Send, {] DownTemp}
	else
		Send, {m DownTemp}
	Return
}
r up::Send, {m Up}


l::z
;+l::Send, {Z DownTemp}
;l::
;{
;	if GetKeyState("CapsLock", "T")
;		Send, {| DownTemp}
;	else if GetKeyState("CapsLock", "T")
;		Send, {V DownTemp}
;	else
;		Send, {v DownTemp}
;	Return
;}
;l up::Send, {v Up}


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Home row

`::Send, {-}

+a::Send, {! DownTemp}
a::
{
	if GetKeyState("CapsLock", "T")
		Send, {\ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {7 DownTemp}
	else
		Send, {, DownTemp}
	Return
}
a up::Send, {, Up}


+o::Send, {H DownTemp}
o::
{
	if GetKeyState("CapsLock", "T")
		Send, {( DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {. DownTemp}
	else
		Send, {h DownTemp}
	Return
}
o up::Send, {h Up}


+e::Send, {E DownTemp}
e::
{
	if GetKeyState("CapsLock", "T")
		Send, {" DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {1 DownTemp}
	else
		Send, {e DownTemp}
	Return
}
e up::Send, {e Up}


+u::Send, {A DownTemp}
u::
{
	if GetKeyState("CapsLock", "T")
		Send, {) DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {0 DownTemp}
	else
		Send, {a DownTemp}
	Return
}
u up::Send, {a Up}


+i::Send, {@ DownTemp}
i::
{
	if GetKeyState("CapsLock", "T")
		Send, {# DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {4 DownTemp}
	else
		Send, {. DownTemp}
	Return
}
i up::Send, {. Up}


+d::Send, {C DownTemp}
d::
{
	if GetKeyState("CapsLock", "T")
		Send, {`% DownTemp}
	else
		Send, {c DownTemp}
	Return
}
d up::Send, {c Up}


+h::Send, {S DownTemp}
h::
{
	if GetKeyState("CapsLock", "T")
		Send, {`{ DownTemp}
	else
		Send, {s DownTemp}
	Return
}
h up::Send, {s Up}


+t::Send, {R DownTemp}
t::
{
	if GetKeyState("CapsLock", "T")
		Send, {= DownTemp}
	else
		Send, {r DownTemp}
	Return
}
t up::Send, {r Up}


+n::Send, {T DownTemp}
n::
{
	if GetKeyState("CapsLock", "T")
		Send, {`} DownTemp}
	else
		Send, {t DownTemp}
	Return
}
n up::Send, {t Up}


+s::Send, {W DownTemp}
s::
{
	if GetKeyState("CapsLock", "T")
		Send, {| DownTemp}
	else
		Send, {w DownTemp}
	Return
}
s up::Send, {w Up}


\::Send, {;}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bottom row

+SC02C::Send, {J DownTemp}
SC02C::
{
	if GetKeyState("NumLock", "T")
		Send, {/ DownTemp}
	else
		Send, {j DownTemp}
	Return
}
SC02C up::Send, {j Up}


+q::Send, {K DownTemp}
q::
{
	if GetKeyState("CapsLock", "T")
		Send, {: DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {6 DownTemp}
	else
		Send, {k DownTemp}
	Return
}
q up::Send, {k Up}


+j::Send, {? DownTemp}
j::
{
	if GetKeyState("CapsLock", "T")
		Send, {* DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {9 DownTemp}
	else
		Send, {/ DownTemp}
	Return
}
j up::Send, {/ Up}


+k::Send, {I DownTemp}
k::
{
	if GetKeyState("CapsLock", "T")
		Send, {+ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {8 DownTemp}
	else
		Send, {i DownTemp}
	Return
}
k up::Send, {i Up}


+x::Send, {`` DownTemp}
x::
{
	if GetKeyState("NumLock", "T")
		Send, {, DownTemp}
	else
		Send, {' DownTemp}
	Return
}
x up::Send, {' Up}


;+b::Send, {G DownTemp}
;b::
;{
;	if GetKeyState("CapsLock", "T")
;		Send, {G DownTemp}
;	else if GetKeyState("CapsLock", "T")
;		Send, {^ DownTemp}
;	else
;		Send, {g DownTemp}
;	Return
;}
;b up::Send, {g Up}


+m::Send, {P DownTemp}
m::
{
	if GetKeyState("CapsLock", "T")
		Send, {& DownTemp}
	else
		Send, {p DownTemp}
	Return
}
m up::Send, {p Up}


+w::Send, {L DownTemp}
w::
{
	if GetKeyState("CapsLock", "T")
		Send, {^ DownTemp}
	else
		Send, {l DownTemp}
	Return
}
w up::Send, {l Up}


+v::Send, {F DownTemp}
v::
{
	if GetKeyState("CapsLock", "T")
		Send, {~ DownTemp}
	else
		Send, {f DownTemp}
	Return
}
v up::Send, {f Up}


z::v



#UseHook off

;
