#SingleInstance force
;#InstallKeybdHook
#UseHook on

; https://ieants.cc/smf/index.php?topic=186.msg2719#msg2719

;Punc
;  +\@  ~^
;  <$>  [_]
; :(-)# `{;}|
;  * /   &=%
;
;Num
;  +/*:
; -523=
; 7.104
; ,689
;
;L0
; 32104 76598
; qyoux mrdgb
; kiea, lntsw
; j" .z fhcpv
;
;S0
; QYOUX   MRDGB
; KIEA!  LNTSW
; J? 'Z  FHCPV
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Variables

LAYOUT_LAYER := "l0"

LAYOUT_KEYS := {}
; Upper Row
; SC009 = '8'
LAYOUT_KEYS["SC002"] := { l0: "3", num: "{tab}", punc: "{tab}" }
LAYOUT_KEYS["+SC002"] := { l0: "+{tab}", num: "+{tab}", punc: "+{tab}" }
LAYOUT_KEYS["SC003"] := { l0: "2", num: "{+}", punc: "{+}" }
LAYOUT_KEYS["+SC003"] := { l0: "2", num: "{+}", punc: "{+}" }
LAYOUT_KEYS["SC004"] := { l0: "1", num: "/", punc: "\" }
LAYOUT_KEYS["+SC004"] := { l0: "1", num: "/", punc: "\" }
LAYOUT_KEYS["SC005"] := { l0: "{0}", num: "*", punc: "@" }
LAYOUT_KEYS["+SC005"] := { l0: "{0}", num: "*", punc: "@" }
LAYOUT_KEYS["SC006"] := { l0: "4", num: ":", punc: ":" }
LAYOUT_KEYS["+SC006"] := { l0: "4", num: ":", punc: ":" }
LAYOUT_KEYS["SC007"] := { l0: "7", num: "7", punc: "7" }
LAYOUT_KEYS["+SC007"] := { l0: "7", num: "7", punc: "7" }
LAYOUT_KEYS["SC008"] := { l0: "6", num: "~", punc: "~" }
LAYOUT_KEYS["+SC008"] := { l0: "6", num: "~", punc: "~" }
LAYOUT_KEYS["SC009"] := { l0: "5", num: "{^}", punc: "{^}" }
LAYOUT_KEYS["+SC009"] := { l0: "5", num: "{^}", punc: "{^}" }
LAYOUT_KEYS["SC00A"] := { l0: "9", num: "9", punc: "9" }
LAYOUT_KEYS["+SC00A"] := { l0: "9", num: "9", punc: "9" }
LAYOUT_KEYS["SC00B"] := { l0: "8", num: "8", punc: "8" }
LAYOUT_KEYS["+SC00B"] := { l0: "8", num: "8", punc: "8" }

; Top Row
LAYOUT_KEYS["'"] := { l0: "q", num: "-", punc: "{tab}" }
LAYOUT_KEYS["+'"] := { l0: "Q", num: "%", punc: "+{tab}" }
LAYOUT_KEYS[","] := { l0: "y", num: "5", punc: "<" }
LAYOUT_KEYS["+,"] := { l0: "Y", num: "$", punc: "<" }
LAYOUT_KEYS["."] := { l0: "o", num: "2", punc: "$" }
LAYOUT_KEYS["+."] := { l0: "O", num: "{#}", punc: "$" }
LAYOUT_KEYS["p"] := { l0: "u", num: "3", punc: ">" }
LAYOUT_KEYS["+p"] := { l0: "U", num: ":", punc: ">" }
LAYOUT_KEYS["y"] := { l0: "x", num: "=", punc: ":" }
LAYOUT_KEYS["+y"] := { l0: "X", num: "\", punc: ":" }
LAYOUT_KEYS["f"] := { l0: "m", num: "{{}", punc: "{{}" }
LAYOUT_KEYS["+f"] := { l0: "M", num: "{{}", punc: "{{}" }
LAYOUT_KEYS["g"] := { l0: "r", num: "[", punc: "[" }
LAYOUT_KEYS["+g"] := { l0: "R", num: "[", punc: "[" }
LAYOUT_KEYS["c"] := { l0: "d", num: "_", punc: "_" }
LAYOUT_KEYS["+C"] := { l0: "D", num: "_", punc: "_" }
LAYOUT_KEYS["r"] := { l0: "g", num: "]", punc: "]" }
LAYOUT_KEYS["+r"] := { l0: "G", num: "]", punc: "]" }
LAYOUT_KEYS["l"] := { l0: "b", num: "{}}", punc: "{}}" }
LAYOUT_KEYS["+L"] := { l0: "B", num: "{}}", punc: "{}}" }

; Home Row
LAYOUT_KEYS["a"] := { l0: "k", num: "7", punc: ":" }
LAYOUT_KEYS["+a"] := { l0: "K", num: "&", punc: ":" }
LAYOUT_KEYS["o"] := { l0: "i", num: ".", punc: "(" }
LAYOUT_KEYS["+o"] := { l0: "I", num: "$", punc: "(" }
LAYOUT_KEYS["e"] := { l0: "e", num: "1", punc: "-" }
LAYOUT_KEYS["+e"] := { l0: "E", num: "{#}", punc: "-" }
LAYOUT_KEYS["u"] := { l0: "a", num: "{0}", punc: ")" }
LAYOUT_KEYS["+u"] := { l0: "A", num: "%", punc: ")" }
LAYOUT_KEYS["i"] := { l0: ",", num: "4", punc: "{#}" }
LAYOUT_KEYS["+i"] := { l0: "{!}", num: "{^}", punc: "{#}" }
LAYOUT_KEYS["d"] := { l0: "l", num: "*", punc: "``" }
LAYOUT_KEYS["+d"] := { l0: "L", num: "*", punc: "``" }
LAYOUT_KEYS["h"] := { l0: "n", num: "{{}", punc: "{{}" }
LAYOUT_KEYS["+h"] := { l0: "N", num: "{{}", punc: "{{}" }
LAYOUT_KEYS["t"] := { l0: "t", num: """", punc: ";" }
LAYOUT_KEYS["+t"] := { l0: "T", num: """", punc: ";" }
LAYOUT_KEYS["n"] := { l0: "s", num: "{}}", punc: "{}}" }
LAYOUT_KEYS["+n"] := { l0: "S", num: "{}}", punc: "{}}" }
LAYOUT_KEYS["s"] := { l0: "w", num: "|", punc: "|" }
LAYOUT_KEYS["+s"] := { l0: "W", num: "|", punc: "|" }
LAYOUT_KEYS["\"] := { l0: ";", num: ";", punc: ";" }
LAYOUT_KEYS["+\"] := { l0: ";", num: ";", punc: ";" }

; Bottom Row
LAYOUT_KEYS[";"] := { l0: "j", num: ",", punc: "{tab}" }
LAYOUT_KEYS["+;"] := { l0: "J", num: ";", punc: "+{tab}" }
LAYOUT_KEYS["q"] := { l0: """", num: "6", punc: "*" }
LAYOUT_KEYS["+q"] := { l0: "?", num: "{^}", punc: "*" }
LAYOUT_KEYS["j"] := { l0: "{up}", num: "9", punc: "{up}" }
LAYOUT_KEYS["+j"] := { l0: "+{up}", num: "(", punc: "+{up}" }
LAYOUT_KEYS["k"] := { l0: ".", num: "8", punc: "/" }
LAYOUT_KEYS["+k"] := { l0: "'", num: "*", punc: ":" }
LAYOUT_KEYS["x"] := { l0: "z", num: "{enter}", punc: "{enter}" }
LAYOUT_KEYS["+x"] := { l0: "Z", num: "+{enter}", punc: "+{enter}" }
LAYOUT_KEYS["b"] := { l0: "f", num: "{^}", punc: "9" }
LAYOUT_KEYS["+b"] := { l0: "F", num: "{^}", punc: "(" }
LAYOUT_KEYS["m"] := { l0: "h", num: "(", punc: "&" }
LAYOUT_KEYS["+m"] := { l0: "H", num: "(", punc: "&" }
LAYOUT_KEYS["w"] := { l0: "c", num: "&", punc: "=" }
LAYOUT_KEYS["+w"] := { l0: "C", num: "&", punc: "=" }
LAYOUT_KEYS["v"] := { l0: "p", num: ")", punc: "%" }
LAYOUT_KEYS["+v"] := { l0: "P", num: ")", punc: "%" }
LAYOUT_KEYS["z"] := { l0: "v", num: "%", punc: "~" }
LAYOUT_KEYS["+z"] := { l0: "V", num: "%", punc: "~" }

LAYOUT_TOGGLERS := { capslock: "punc", numlock: "num", scrollLock: "l0" }

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; FUNCTIONS

BEAKL_RunKey(key) {
	global LAYOUT_LAYER, LAYOUT_KEYS

	If % (LAYOUT_KEYS[key][LAYOUT_LAYER]) {
		Send % LAYOUT_KEYS[key][LAYOUT_LAYER]
	}
	else {
		Send % key
	}
}

BEAKL_ToggleLayer(k) {
	global LAYOUT_LAYER, LAYOUT_TOGGLERS
	layer := LAYOUT_TOGGLERS[k]

	; turn off lights
	SetNumLockState , off
	;SetCapsLockState , off
	SetScrollLockState , off

	If (LAYOUT_LAYER = layer) {
		LAYOUT_LAYER := "l0"
	}
	else {
		LAYOUT_LAYER := layer

		; turn on lights

		if (LAYOUT_LAYER = "num") {
			SetNumLockState, on
		}
		else if (LAYOUT_LAYER = "punc") {
			SetScrollLockState, on
		}
	}
}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;; Input replacements

; Toggle layers

For k, v in LAYOUT_TOGGLERS {
	Hotkey %k%, LABEL_ToggleLayer
}

; Main Chars

For k, v in LAYOUT_KEYS {
	Hotkey %k%, LABEL_RunKey
}

; Backspace

l::send, {backspace}
+l::send, {delete}

LABEL_RunKey:
	k := A_ThisHotkey
	BEAKL_RunKey(k)
	return

LABEL_ToggleLayer:
	k := A_ThisHotkey
	BEAKL_ToggleLayer(k)
	return

