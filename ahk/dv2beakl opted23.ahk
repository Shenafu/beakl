#SingleInstance force
;#InstallKeybdHook
#UseHook on
#IfWinNotActive, ahk_class VMPlayerFrame
#IfWinNotActive, ahk_exe dosbox.exe

;Move
;								WheelUp
;	Tab			PgUp		Up     			Enter	Copy
;	Home		Left		Down    		Right	End
;	R-Click		PgDn		WheelDown	Click		Paste
;
;Punc
;   =+  ^
;  <$>  [_]
; \(-)= %{;}`
;  *:/   |~&
;
;Num
;  +/*:
; -523=  FED
; 7.104  CBA
; ,689     %
;
;L0
;  32104 76598
;  xyou' wdncq
;- khea. gtrsm ;
;  j,"iz bplfv
;
;SNum
; +#*: 7~^98
; %$#:\ {FED}
; &$#%^ &CBA|
; ;^(*  ^(%)%
;
;S0
;  3!#$% 76598
;  XYOU` WDNCQ
;- KHEA@ GTRSM ;
;  J?!IZ BPLFV

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Variables

LAYOUT_LAYER := "l0"

LAYOUT_KEYS := {}
; Upper Row
; SC009 = '8'
LAYOUT_KEYS["SC002"] := { l0: "3", num: "{tab}", punc: "{tab}" }
LAYOUT_KEYS["+SC002"] := { l0: "+{tab}", num: "+{tab}", punc: "+{tab}" }
LAYOUT_KEYS["SC003"] := { l0: "2", num: "{+}", punc: "{+}" }
LAYOUT_KEYS["+SC003"] := { l0: "{!}", num: "{!}", punc: "{+}" }
LAYOUT_KEYS["SC004"] := { l0: "1", num: "/", punc: "=", move: "{WheelUp}" }
LAYOUT_KEYS["+SC004"] := { l0: "{#}", num: "{#}", punc: "=", move: "+{WheelUp}" }
LAYOUT_KEYS["SC005"] := { l0: "{0}", num: "*", punc: "{+}" }
LAYOUT_KEYS["+SC005"] := { l0: "$", num: "$", punc: "{+}" }
LAYOUT_KEYS["SC006"] := { l0: "4", num: ":", punc: ":" }
LAYOUT_KEYS["+SC006"] := { l0: "%", num: ":", punc: ":" }
LAYOUT_KEYS["SC007"] := { l0: "7", num: "7", punc: "7" }
LAYOUT_KEYS["+SC007"] := { l0: "7", num: "7", punc: "7" }
LAYOUT_KEYS["SC008"] := { l0: "6", num: "~", punc: "%" }
LAYOUT_KEYS["+SC008"] := { l0: "6", num: "~", punc: "%" }
LAYOUT_KEYS["SC009"] := { l0: "5", num: "{^}", punc: "{^}" }
LAYOUT_KEYS["+SC009"] := { l0: "5", num: "{^}", punc: "{^}" }
LAYOUT_KEYS["SC00A"] := { l0: "9", num: "9", punc: "9" }
LAYOUT_KEYS["+SC00A"] := { l0: "9", num: "9", punc: "9" }
LAYOUT_KEYS["SC00B"] := { l0: "8", num: "8", punc: "8" }
LAYOUT_KEYS["+SC00B"] := { l0: "8", num: "8", punc: "8" }

; Top Row
LAYOUT_KEYS["'"] := { l0: "x", num: "-", punc: "{tab}", move: "{Tab}" }
LAYOUT_KEYS["+'"] := { l0: "X", num: "%", punc: "+{tab}", move: "+{Tab}" }
LAYOUT_KEYS[","] := { l0: "y", num: "5", punc: "<", move: "{PgUp}" }
LAYOUT_KEYS["+,"] := { l0: "Y", num: "$", punc: "<", move: "+{PgUp}" }
LAYOUT_KEYS["."] := { l0: "o", num: "2", punc: "$", move: "{Up}"  }
LAYOUT_KEYS["+."] := { l0: "O", num: "{#}", punc: "$", move: "+{Up}" }
LAYOUT_KEYS["p"] := { l0: "u", num: "3", punc: ">", move: "{Enter}" }
LAYOUT_KEYS["+p"] := { l0: "U", num: ":", punc: ">", move: "+{Enter}" }
LAYOUT_KEYS["y"] := { l0: "'", num: "=", punc: ":", move: "^c" }
LAYOUT_KEYS["+y"] := { l0: "``", num: "\", punc: ":", move: "+^c" }
LAYOUT_KEYS["f"] := { l0: "w", num: "[", punc: "{{}" }
LAYOUT_KEYS["+f"] := { l0: "W", num: "{{}", punc: "{{}" }
LAYOUT_KEYS["g"] := { l0: "d", num: "F", punc: "[" }
LAYOUT_KEYS["+g"] := { l0: "D", num: "F", punc: "[" }
LAYOUT_KEYS["c"] := { l0: "n", num: "E", punc: "_" }
LAYOUT_KEYS["+C"] := { l0: "N", num: "E", punc: "_" }
LAYOUT_KEYS["r"] := { l0: "c", num: "D", punc: "]" }
LAYOUT_KEYS["+r"] := { l0: "C", num: "D", punc: "]" }
LAYOUT_KEYS["l"] := { l0: "q", num: "]", punc: "{}}" }
LAYOUT_KEYS["+L"] := { l0: "Q", num: "{}}", punc: "{}}" }

; Home Row
LAYOUT_KEYS["a"] := { l0: "k", num: "7", punc: "\", move: "{Home}" }
LAYOUT_KEYS["+a"] := { l0: "K", num: "&", punc: "\", move: "+{Home}" }
LAYOUT_KEYS["o"] := { l0: "h", num: ".", punc: "(", move: "{Left}" }
LAYOUT_KEYS["+o"] := { l0: "H", num: "$", punc: "(", move: "+{Left}" }
LAYOUT_KEYS["e"] := { l0: "e", num: "1", punc: "-", move: "{Down}" }
LAYOUT_KEYS["+e"] := { l0: "E", num: "{#}", punc: "-", move: "+{Down}" }
LAYOUT_KEYS["u"] := { l0: "a", num: "{0}", punc: ")", move: "{Right}" }
LAYOUT_KEYS["+u"] := { l0: "A", num: "%", punc: ")", move: "+{Right}" }
LAYOUT_KEYS["i"] := { l0: ".", num: "4", punc: "=", move: "{End}" }
LAYOUT_KEYS["+i"] := { l0: "@", num: "{^}", punc: "=", move: "+{End}" }
LAYOUT_KEYS["d"] := { l0: "g", num: "$", punc: "%" }
LAYOUT_KEYS["+d"] := { l0: "G", num: "&", punc: "%" }
LAYOUT_KEYS["h"] := { l0: "t", num: "C", punc: "{{}" }
LAYOUT_KEYS["+h"] := { l0: "T", num: "C", punc: "{{}" }
LAYOUT_KEYS["t"] := { l0: "r", num: "B", punc: ";" }
LAYOUT_KEYS["+t"] := { l0: "R", num: "B", punc: ";" }
LAYOUT_KEYS["n"] := { l0: "s", num: "A", punc: "{}}" }
LAYOUT_KEYS["+n"] := { l0: "S", num: "A", punc: "{}}" }
LAYOUT_KEYS["s"] := { l0: "m", num: "{#}", punc: "``" }
LAYOUT_KEYS["+s"] := { l0: "M", num: "|", punc: "``" }
LAYOUT_KEYS["\"] := { l0: ";", num: ";", punc: ";" }
LAYOUT_KEYS["+\"] := { l0: ";", num: ";", punc: ";" }

; Bottom Row
LAYOUT_KEYS[";"] := { l0: "j", num: ",", punc: "{tab}", move: "{RButton}" }
LAYOUT_KEYS["+;"] := { l0: "J", num: ";", punc: "+{tab}", move: "+{RButton}" }
LAYOUT_KEYS["q"] := { l0: ",", num: "6", punc: "*", move: "{PgDn}" }
LAYOUT_KEYS["+q"] := { l0: "?", num: "{^}", punc: "*", move: "+{PgDn}" }
LAYOUT_KEYS["j"] := { l0: """", num: "9", punc: ":", move: "{WheelDown}" }
LAYOUT_KEYS["+j"] := { l0: "{!}", num: "(", punc: ":", move: "+{WheelDown}" }
LAYOUT_KEYS["k"] := { l0: "i", num: "8", punc: "/", move: "{LButton}" }
LAYOUT_KEYS["+k"] := { l0: "I", num: "*", punc: "/", move: "+{LButton}" }
LAYOUT_KEYS["x"] := { l0: "z", num: "{enter}", punc: "{enter}", move: "^v" }
LAYOUT_KEYS["+x"] := { l0: "Z", num: "+{enter}", punc: "+{enter}", move: "+^v" }
LAYOUT_KEYS["b"] := { l0: "b", num: "x", punc: "(" }
LAYOUT_KEYS["+b"] := { l0: "B", num: "X", punc: "(" }
LAYOUT_KEYS["m"] := { l0: "p", num: "(", punc: "|" }
LAYOUT_KEYS["+m"] := { l0: "P", num: "(", punc: "|" }
LAYOUT_KEYS["w"] := { l0: "l", num: "%", punc: "~" }
LAYOUT_KEYS["+w"] := { l0: "L", num: "%", punc: "~" }
LAYOUT_KEYS["v"] := { l0: "f", num: ")", punc: "&" }
LAYOUT_KEYS["+v"] := { l0: "F", num: ")", punc: "&" }
LAYOUT_KEYS["z"] := { l0: "v", num: "y", punc: ")" }
LAYOUT_KEYS["+z"] := { l0: "V", num: "Y", punc: ")" }

LAYOUT_TOGGLERS := { capslock: "punc", numlock: "num", scrollLock: "move" }

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; FUNCTIONS

BEAKL_RunKey(key) {
	global LAYOUT_LAYER, LAYOUT_KEYS

	If % (LAYOUT_KEYS[key][LAYOUT_LAYER]) {
		Send % LAYOUT_KEYS[key][LAYOUT_LAYER]
	}
	else {
		Send % key
	}
}

BEAKL_ToggleLayer(k) {
	global LAYOUT_LAYER, LAYOUT_TOGGLERS
	layer := LAYOUT_TOGGLERS[k]

	; turn off lights
	SetNumLockState , off
	SetCapsLockState , off
	SetScrollLockState , off

	If (LAYOUT_LAYER = layer) {
		LAYOUT_LAYER := "l0"
	}
	else {
		LAYOUT_LAYER := layer

		; turn on lights

		if (LAYOUT_LAYER = "num") {
			SetNumLockState, on
		}
		else if (LAYOUT_LAYER = "punc") {
			SetCapsLockState, on
		}
		else if (LAYOUT_LAYER = "move") {
			SetScrollLockState, on
		}
	}
}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;; Input replacements

; Toggle layers

For k, v in LAYOUT_TOGGLERS {
	Hotkey %k%, LABEL_ToggleLayer
}

; Main Chars

For k, v in LAYOUT_KEYS {
	Hotkey %k%, LABEL_RunKey
}

; Backspace

l::send, {backspace}
+l::send, {delete}

LABEL_RunKey:
	k := A_ThisHotkey
	BEAKL_RunKey(k)
	return

LABEL_ToggleLayer:
	k := A_ThisHotkey
	BEAKL_ToggleLayer(k)
	return

