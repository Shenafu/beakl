; Balanced Effortless Advanced Keyboard Layout
; convert dvorak to the BEAKL Opted layout

;UNSHIFTED
;j"'.x wfmbz
;qyouk gclrv
;hiea, dstnp

;SHIFTED
;J(;)X WFMBZ
;QYOUK GCLRV
;HIEA: DSTNP

#SingleInstance force
;#InstallKeybdHook
#UseHook on

;; Top row
'::Send, j
,::Send, "
.::Send, '
p::Send, .
y::Send, x
f::Send, w
g::Send, f
c::Send, m
r::Send, b
l::Send, z

+'::Send, J
+,::Send, {(}
+.::Send, {;}
+p::Send, {)}
+y::Send, X
+f::Send, W
+g::Send, F
+c::Send, M
+r::Send, B
+l::Send, Z

;; Home row
a::Send, q
o::Send, y
e::Send, o
u::Send, u
i::Send, k
d::Send, g
h::Send, c
t::Send, l
n::Send, r
s::Send, v

+a::Send, Q
+o::Send, Y
+e::Send, O
+u::Send, U
+i::Send, K
+d::Send, G
+h::Send, C
+t::Send, L
+n::Send, R
+s::Send, V

;; Bottom row
SC02C::Send, h
q::Send, i
j::Send, e
k::Send, a
x::Send, ,
b::Send, d
m::Send, s
w::Send, t
v::Send, n
z::Send, p

+SC02C::Send, H
+q::Send, I
+j::Send, E
+k::Send, A
+x::Send, :
+b::Send, D
+m::Send, S
+w::Send, T
+v::Send, N
+z::Send, P

;; Number row
;`::Send, =
1::Send, 4
2::Send, 0
3::Send, 1
4::Send, 2
5::Send, 3
6::Send, 8
7::Send, 7
8::Send, 6
9::Send, 5
0::Send, 9
;[::Send, /
;]::Send, \

;+`::Send, {SC029} ; backquote
;+1::Send, +2
;+2::Send, +[
;+3::Send, +/
;+4::Send, +5
;+5::Send, {+}
;+6::Send, +8
;+7::Send, +7
;+8::Send, +1
+9::Send, <
+0::Send, >
;+[::Send, +6
;+]::Send, +4

#UseHook off

;
