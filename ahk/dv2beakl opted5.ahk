; Balanced Effortless Advanced Keyboard Layout
; convert dvorak to the BEAKL Opted layout

;UNSHIFTED
;  qyoux fgrcv
;j hiea. lstnw z
;  '"(,) bdmpk

;SHIFTED
;  QYOUX FGRCV
;J HIEA@ LSTNW Z
;  ?!\:` BDMPK

;ALTGR / SCROLL LOCK
;  &}_{# ~[$]|
;  ;/0-+ =<1>*
;  6789% ^2345



#SingleInstance force
;#InstallKeybdHook
#UseHook on


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Top row


+`::Send, {J DownTemp}
`::
{
	if GetKeyState("NumLock", "T")
		Send, { tab DownTemp }
	else if GetKeyState("CapsLock", "T")
		Send, {J DownTemp}
	else
		Send, {j DownTemp}
	Return
}
` up::Send, {j Up}


+'::Send, {Q DownTemp}
'::
{
	if GetKeyState("NumLock", "T")
		Send, {- DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {& DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {Q DownTemp}
	else
		Send, {q DownTemp}
	Return
}
' up::Send, {q Up}


+,::Send, {Y DownTemp}
,::
{
	if GetKeyState("NumLock", "T")
		Send, {3 DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {`} DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {Y DownTemp}
	else
		Send, {y DownTemp}
	Return
}
, up::Send, {y Up}


+.::Send, {O DownTemp}
.::
{
	if GetKeyState("NumLock", "T")
		Send, {9 DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {_ DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {O DownTemp}
	else
		Send, {o DownTemp}
	Return
}
. up::Send, {o Up}


+p::Send, {U DownTemp}
p::
{
	if GetKeyState("NumLock", "T")
		Send, {7 DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {`{ DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {U DownTemp}
	else
		Send, {u DownTemp}
	Return
}
p up::Send, {u Up}


+y::Send, {X DownTemp}
y::
{
	if GetKeyState("NumLock", "T")
		Send, {6 DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {# DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {X DownTemp}
	else
		Send, {x DownTemp}
	Return
}
y up::Send, {x Up}


+f::Send, {F DownTemp}
f::
{
	if GetKeyState("ScrollLock", "T")
		Send, {~ DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {F DownTemp}
	else
		Send, {f DownTemp}
	Return
}
f up::Send, {f Up}


+g::Send, {G DownTemp}
g::
{
	if GetKeyState("ScrollLock", "T")
		Send, {[ DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {G DownTemp}
	else
		Send, {g DownTemp}
	Return
}
g up::Send, {g Up}


+c::Send, {R DownTemp}
c::
{
	if GetKeyState("ScrollLock", "T")
		Send, {$ DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {R DownTemp}
	else
		Send, {r DownTemp}
	Return
}
c up::Send, {r Up}


+r::Send, {C DownTemp}
r::
{
	if GetKeyState("ScrollLock", "T")
		Send, {] DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {C DownTemp}
	else
		Send, {c DownTemp}
	Return
}
r up::Send, {c Up}


+l::Send, {V DownTemp}
l::
{
	if GetKeyState("ScrollLock", "T")
		Send, {| DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {V DownTemp}
	else
		Send, {v DownTemp}
	Return
}
l up::Send, {v Up}


+\::Send, {Z DownTemp}
\::
{
	if GetKeyState("CapsLock", "T")
		Send, {Z DownTemp}
	else
		Send, {z DownTemp}
	Return
}
\ up::Send, {z Up}



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Home row

+a::Send, {H DownTemp}
a::
{
	if GetKeyState("NumLock", "T")
		Send, {5 DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {`; DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {H DownTemp}
	else
		Send, {h DownTemp}
	Return
}
a up::Send, {h Up}


+o::Send, {I DownTemp}
o::
{
	if GetKeyState("NumLock", "T")
		Send, {1 DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {/ DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {I DownTemp}
	else
		Send, {i DownTemp}
	Return
}
o up::Send, {i Up}


+e::Send, {E DownTemp}
e::
{
	if GetKeyState("NumLock", "T")
		Send, {0 DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {0 DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {E DownTemp}
	else
		Send, {e DownTemp}
	Return
}
e up::Send, {e Up}


+u::Send, {A DownTemp}
u::
{
	if GetKeyState("NumLock", "T")
		Send, {2 DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {- DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {A DownTemp}
	else
		Send, {a DownTemp}
	Return
}
u up::Send, {a Up}


+i::Send, {@ DownTemp}
i::
{
	if GetKeyState("ScrollLock", "T")
		Send, {+ DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {@ DownTemp}
	else
		Send, {. DownTemp}
	Return
}
i up::Send, {. Up}


+d::Send, {L DownTemp}
d::
{
	if GetKeyState("ScrollLock", "T")
		Send, {= DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {L DownTemp}
	else
		Send, {l DownTemp}
	Return
}
d up::Send, {l Up}


+h::Send, {S DownTemp}
h::
{
	if GetKeyState("ScrollLock", "T")
		Send, {< DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {S DownTemp}
	else
		Send, {s DownTemp}
	Return
}
h up::Send, {s Up}


+t::Send, {T DownTemp}
t::
{
	if GetKeyState("ScrollLock", "T")
		Send, {1 DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {T DownTemp}
	else
		Send, {t DownTemp}
	Return
}
t up::Send, {t Up}


+n::Send, {N DownTemp}
n::
{
	if GetKeyState("ScrollLock", "T")
		Send, {> DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {N DownTemp}
	else
		Send, {n DownTemp}
	Return
}
n up::Send, {n Up}


+s::Send, {W DownTemp}
s::
{
	if GetKeyState("ScrollLock", "T")
		Send, {* DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {W DownTemp}
	else
		Send, {w DownTemp}
	Return
}
s up::Send, {w Up}



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bottom row

+SC02C::Send, {? DownTemp}
SC02C::
{
	if GetKeyState("NumLock", "T")
		Send, {/ DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {6 DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {? DownTemp}
	else
		Send, {' DownTemp}
	Return
}
SC02C up::Send, {' Up}


+q::Send, {! DownTemp}
q::
{
	if GetKeyState("NumLock", "T")
		Send, {4 DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {7 DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {! DownTemp}
	else
		Send, {" DownTemp}
	Return
}
q up::Send, {" Up}


+j::Send, {\ DownTemp}
j::
{
	if GetKeyState("NumLock", "T")
		Send, {8 DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {8 DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {\ DownTemp}
	else
		Send, {( DownTemp}
	Return
}
j up::Send, {( Up}


+k::Send, {: DownTemp}
k::
{
	if GetKeyState("ScrollLock", "T")
		Send, {9 DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {: DownTemp}
	else
		Send, {, DownTemp}
	Return
}
k up::Send, {, Up}


+x::Send, {`` DownTemp}
x::
{
	if GetKeyState("NumLock", "T")
		Send, {: DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {`% DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {`` DownTemp}
	else
		Send, {) DownTemp}
	Return
}
x up::Send, {) Up}


+b::Send, {B DownTemp}
b::
{
	if GetKeyState("ScrollLock", "T")
		Send, {^ DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {B DownTemp}
	else
		Send, {b DownTemp}
	Return
}
b up::Send, {b Up}


+m::Send, {D DownTemp}
m::
{
	if GetKeyState("ScrollLock", "T")
		Send, {2 DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {D DownTemp}
	else
		Send, {d DownTemp}
	Return
}
m up::Send, {d Up}


+w::Send, {M DownTemp}
w::
{
	if GetKeyState("ScrollLock", "T")
		Send, {3 DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {M DownTemp}
	else
		Send, {m DownTemp}
	Return
}
w up::Send, {m Up}


+v::Send, {P DownTemp}
v::
{
	if GetKeyState("ScrollLock", "T")
		Send, {4 DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {P DownTemp}
	else
		Send, {p DownTemp}
	Return
}
v up::Send, {p Up}


+z::Send, {K DownTemp}
z::
{
	if GetKeyState("ScrollLock", "T")
		Send, {5 DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {K DownTemp}
	else
		Send, {k DownTemp}
	Return
}
z up::Send, {k Up}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Number row
1::Send, 4
2::Send, 0
3::Send, 1
4::Send, 2
5::Send, 3
6::Send, 8
7::Send, 7
8::Send, 6
9::Send, 5
0::Send, 9


#UseHook off

;
