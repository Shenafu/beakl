; one-handed keyboard layout


; right-handed
; cf. FrogPad
; FARWP = [TAB]'BMJ
; OEHTD = QZLCV
; UINSY = [ALT][APP]KGX

*g::
SetKeyDelay -1   ; If the destination key is a mouse button, SetMouseDelay is used instead.
Send {Blind}{a DownTemp}  ; DownTemp is like Down except that other Send commands in the script won't assume "b" should stay down during their Send.
return
*g up::
SetKeyDelay -1  ; See note below for why press-duration is not specified with either of these SetKeyDelays.
Send {Blind}{a Up}
return

*c::
SetKeyDelay -1   ; If the destination key is a mouse button, SetMouseDelay is used instead.
Send {Blind}{r DownTemp}  ; DownTemp is like Down except that other Send commands in the script won't assume "b" should stay down during their Send.
return
*c up::
SetKeyDelay -1  ; See note below for why press-duration is not specified with either of these SetKeyDelays.
Send {Blind}{r Up}
return

*r::
SetKeyDelay -1   ; If the destination key is a mouse button, SetMouseDelay is used instead.
Send {Blind}{w DownTemp}  ; DownTemp is like Down except that other Send commands in the script won't assume "b" should stay down during their Send.
return
*r up::
SetKeyDelay -1  ; See note below for why press-duration is not specified with either of these SetKeyDelays.
Send {Blind}{w Up}
return

*l::
SetKeyDelay -1   ; If the destination key is a mouse button, SetMouseDelay is used instead.
Send {Blind}{p DownTemp}  ; DownTemp is like Down except that other Send commands in the script won't assume "b" should stay down during their Send.
return
*l up::
SetKeyDelay -1  ; See note below for why press-duration is not specified with either of these SetKeyDelays.
Send {Blind}{p Up}
return

*d::
SetKeyDelay -1   ; If the destination key is a mouse button, SetMouseDelay is used instead.
Send {Blind}{o DownTemp}  ; DownTemp is like Down except that other Send commands in the script won't assume "b" should stay down during their Send.
return
*d up::
SetKeyDelay -1  ; See note below for why press-duration is not specified with either of these SetKeyDelays.
Send {Blind}{o Up}
return

*h::
SetKeyDelay -1   ; If the destination key is a mouse button, SetMouseDelay is used instead.
Send {Blind}{e DownTemp}  ; DownTemp is like Down except that other Send commands in the script won't assume "b" should stay down during their Send.
return
*h up::
SetKeyDelay -1  ; See note below for why press-duration is not specified with either of these SetKeyDelays.
Send {Blind}{e Up}
return

*t::
SetKeyDelay -1   ; If the destination key is a mouse button, SetMouseDelay is used instead.
Send {Blind}{h DownTemp}  ; DownTemp is like Down except that other Send commands in the script won't assume "b" should stay down during their Send.
return
*t up::
SetKeyDelay -1  ; See note below for why press-duration is not specified with either of these SetKeyDelays.
Send {Blind}{h Up}
return

*n::
SetKeyDelay -1   ; If the destination key is a mouse button, SetMouseDelay is used instead.
Send {Blind}{t DownTemp}  ; DownTemp is like Down except that other Send commands in the script won't assume "b" should stay down during their Send.
return
*n up::
SetKeyDelay -1  ; See note below for why press-duration is not specified with either of these SetKeyDelays.
Send {Blind}{t Up}
return

*s::
SetKeyDelay -1   ; If the destination key is a mouse button, SetMouseDelay is used instead.
Send {Blind}{d DownTemp}  ; DownTemp is like Down except that other Send commands in the script won't assume "b" should stay down during their Send.
return
*s up::
SetKeyDelay -1  ; See note below for why press-duration is not specified with either of these SetKeyDelays.
Send {Blind}{d Up}
return

*b::
SetKeyDelay -1   ; If the destination key is a mouse button, SetMouseDelay is used instead.
Send {Blind}{u DownTemp}  ; DownTemp is like Down except that other Send commands in the script won't assume "b" should stay down during their Send.
return
*b up::
SetKeyDelay -1  ; See note below for why press-duration is not specified with either of these SetKeyDelays.
Send {Blind}{u Up}
return

*m::
SetKeyDelay -1   ; If the destination key is a mouse button, SetMouseDelay is used instead.
Send {Blind}{i DownTemp}  ; DownTemp is like Down except that other Send commands in the script won't assume "b" should stay down during their Send.
return
*m up::
SetKeyDelay -1  ; See note below for why press-duration is not specified with either of these SetKeyDelays.
Send {Blind}{i Up}
return

*w::
SetKeyDelay -1   ; If the destination key is a mouse button, SetMouseDelay is used instead.
Send {Blind}{n DownTemp}  ; DownTemp is like Down except that other Send commands in the script won't assume "b" should stay down during their Send.
return
*w up::
SetKeyDelay -1  ; See note below for why press-duration is not specified with either of these SetKeyDelays.
Send {Blind}{n Up}
return

*v::
SetKeyDelay -1   ; If the destination key is a mouse button, SetMouseDelay is used instead.
Send {Blind}{s DownTemp}  ; DownTemp is like Down except that other Send commands in the script won't assume "b" should stay down during their Send.
return
*v up::
SetKeyDelay -1  ; See note below for why press-duration is not specified with either of these SetKeyDelays.
Send {Blind}{s Up}
return

*z::
SetKeyDelay -1   ; If the destination key is a mouse button, SetMouseDelay is used instead.
Send {Blind}{y DownTemp}  ; DownTemp is like Down except that other Send commands in the script won't assume "b" should stay down during their Send.
return
*z up::
SetKeyDelay -1  ; See note below for why press-duration is not specified with either of these SetKeyDelays.
Send {Blind}{y Up}
return

