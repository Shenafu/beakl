; Balanced Effortless Advanced Keyboard Layout
; convert dvorak to the BEAKL Opted layout

;UNSHIFTED
;  40123 76598
;  qhoux gdnmk
;- ,aeiy csrtw ;
;  z.'/j bplfv

;SHIFTED
;         ^%~
;  QHOUX GDNMK
;- !AEIY CSRTW ;
;  Z@`?J BPLFV

;NUM LOCK
;   +=*
;   794,
;- 80123
;  6/5.:

;ALTGR / SCROLL LOCK
;   +=*
;   ["]& {_}
;  \(1)# $<0>|
;  5432:  9876


#SingleInstance force
;#InstallKeybdHook
#UseHook on


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Number row

1::Send, 4


+2::Send, {+}
2::
{
	if GetKeyState("ScrollLock", "T")
		Send, {+ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {+ DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {+ DownTemp}
	else
		Send, {0 DownTemp}
	Return
}
2 up::Send, {0 Up}


+3::Send, {=}
3::
{
	if GetKeyState("ScrollLock", "T")
		Send, {= DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {= DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {= DownTemp}
	else
		Send, {1 DownTemp}
	Return
}
3 up::Send, {1 Up}


+4::Send, {*}
4::
{
	if GetKeyState("ScrollLock", "T")
		Send, {* DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {* DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {* DownTemp}
	else
		Send, {2 DownTemp}
	Return
}
4 up::Send, {2 Up}


5::Send, 3
6::Send, 7


+7::Send, {^}
7::
{
	if GetKeyState("ScrollLock", "T")
		Send, {^ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {^ DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {^ DownTemp}
	else
		Send, {6 DownTemp}
	Return
}
7 up::Send, {6 Up}


+8::Send, {`%}
8::
{
	if GetKeyState("ScrollLock", "T")
		Send, {`% DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {`% DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {`% DownTemp}
	else
		Send, {5 DownTemp}
	Return
}
8 up::Send, {5 Up}


+9::Send, {~}
9::
{
	if GetKeyState("ScrollLock", "T")
		Send, {~ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {~ DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {~ DownTemp}
	else
		Send, {9 DownTemp}
	Return
}
9 up::Send, {9 Up}


0::Send, 8


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Top row

+'::Send, {Q DownTemp}
'::
{
	if GetKeyState("ScrollLock", "T")
		Send, {tab DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {tab DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {Q DownTemp}
	else
		Send, {q DownTemp}
	Return
}
' up::Send, {q Up}


+,::Send, {H DownTemp}
,::
{
	if GetKeyState("ScrollLock", "T")
		Send, {[ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {7 DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {H DownTemp}
	else
		Send, {h DownTemp}
	Return
}
, up::Send, {h Up}


+.::Send, {O DownTemp}
.::
{
	if GetKeyState("ScrollLock", "T")
		Send, {" DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {9 DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {O DownTemp}
	else
		Send, {o DownTemp}
	Return
}
. up::Send, {o Up}


+p::Send, {U DownTemp}
p::
{
	if GetKeyState("ScrollLock", "T")
		Send, {] DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {4 DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {U DownTemp}
	else
		Send, {u DownTemp}
	Return
}
p up::Send, {u Up}


+y::Send, {X DownTemp}
y::
{
	if GetKeyState("ScrollLock", "T")
		Send, {& DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {, DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {X DownTemp}
	else
		Send, {x DownTemp}
	Return
}
y up::Send, {x Up}


f::Send, g
+f::Send, {G DownTemp}
;f::
;{
;	if GetKeyState("ScrollLock", "T")
;		Send, {& DownTemp}
;	else if GetKeyState("CapsLock", "T")
;		Send, {G DownTemp}
;	else
;		Send, {g DownTemp}
;	Return
;}
;f up::Send, {g Up}


+g::Send, {D DownTemp}
g::
{
	if GetKeyState("ScrollLock", "T")
		Send, {`{ DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {D DownTemp}
	else
		Send, {d DownTemp}
	Return
}
g up::Send, {d Up}


+c::Send, {N DownTemp}
c::
{
	if GetKeyState("ScrollLock", "T")
		Send, {_ DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {N DownTemp}
	else
		Send, {n DownTemp}
	Return
}
c up::Send, {n Up}


+r::Send, {M DownTemp}
r::
{
	if GetKeyState("ScrollLock", "T")
		Send, {`} DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {M DownTemp}
	else
		Send, {m DownTemp}
	Return
}
r up::Send, {m Up}


l::Send, k
+l::Send, {K DownTemp}
;l::
;{
;	if GetKeyState("ScrollLock", "T")
;		Send, {| DownTemp}
;	else if GetKeyState("CapsLock", "T")
;		Send, {K DownTemp}
;	else
;		Send, {k DownTemp}
;	Return
;}
;l up::Send, {k Up}


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Home row

`::Send, {-}

+a::Send, {! DownTemp}
a::
{
	if GetKeyState("CapsLock", "T")
		Send, {! DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {\ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {8 DownTemp}
	else
		Send, {, DownTemp}
	Return
}
a up::Send, {, Up}


+o::Send, {A DownTemp}
o::
{
	if GetKeyState("CapsLock", "T")
		Send, {A DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {( DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {0 DownTemp}
	else
		Send, {a DownTemp}
	Return
}
o up::Send, {a Up}


+e::Send, {E DownTemp}
e::
{
	if GetKeyState("CapsLock", "T")
		Send, {E DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {1 DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {1 DownTemp}
	else
		Send, {e DownTemp}
	Return
}
e up::Send, {e Up}


+u::Send, {I DownTemp}
u::
{
	if GetKeyState("CapsLock", "T")
		Send, {I DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {) DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {2 DownTemp}
	else
		Send, {i DownTemp}
	Return
}
u up::Send, {i Up}


+i::Send, {Y DownTemp}
i::
{
	if GetKeyState("CapsLock", "T")
		Send, {Y DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {# DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {3 DownTemp}
	else
		Send, {y DownTemp}
	Return
}
i up::Send, {y Up}


+d::Send, {C DownTemp}
d::
{
	if GetKeyState("CapsLock", "T")
		Send, {C DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {$ DownTemp}
	else
		Send, {c DownTemp}
	Return
}
d up::Send, {c Up}


+h::Send, {S DownTemp}
h::
{
	if GetKeyState("CapsLock", "T")
		Send, {S DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {< DownTemp}
	else
		Send, {s DownTemp}
	Return
}
h up::Send, {s Up}


+t::Send, {R DownTemp}
t::
{
	if GetKeyState("CapsLock", "T")
		Send, {R DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {0 DownTemp}
	else
		Send, {r DownTemp}
	Return
}
t up::Send, {r Up}


+n::Send, {T DownTemp}
n::
{
	if GetKeyState("CapsLock", "T")
		Send, {T DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {> DownTemp}
	else
		Send, {t DownTemp}
	Return
}
n up::Send, {t Up}


+s::Send, {W DownTemp}
s::
{
	if GetKeyState("CapsLock", "T")
		Send, {W DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {| DownTemp}
	else
		Send, {w DownTemp}
	Return
}
s up::Send, {w Up}


\::Send, {;}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bottom row

+SC02C::Send, {Z DownTemp}
SC02C::
{
	if GetKeyState("CapsLock", "T")
		Send, {Z DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {5 DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {6 DownTemp}
	else
		Send, {z DownTemp}
	Return
}
SC02C up::Send, {z Up}


+q::Send, {@ DownTemp}
q::
{
	if GetKeyState("CapsLock", "T")
		Send, {@ DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {4 DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {/ DownTemp}
	else
		Send, {. DownTemp}
	Return
}
q up::Send, {. Up}


+j::Send, {`` DownTemp}
j::
{
	if GetKeyState("CapsLock", "T")
		Send, {`` DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {3 DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {5 DownTemp}
	else
		Send, {' DownTemp}
	Return
}
j up::Send, {' Up}


+k::Send, {? DownTemp}
k::
{
	if GetKeyState("CapsLock", "T")
		Send, {? DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {2 DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {. DownTemp}
	else
		Send, {/ DownTemp}
	Return
}
k up::Send, {/ Up}


+x::Send, {J DownTemp}
x::
{
	if GetKeyState("CapsLock", "T")
		Send, {J DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {: DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {: DownTemp}
	else
		Send, {j DownTemp}
	Return
}
x up::Send, {j Up}


;+b::Send, {B DownTemp}
;b::
;{
;	if GetKeyState("CapsLock", "T")
;		Send, {B DownTemp}
;	else if GetKeyState("ScrollLock", "T")
;		Send, {^ DownTemp}
;	else
;		Send, {b DownTemp}
;	Return
;}
;b up::Send, {b Up}


+m::Send, {P DownTemp}
m::
{
	if GetKeyState("CapsLock", "T")
		Send, {P DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {9 DownTemp}
	else
		Send, {p DownTemp}
	Return
}
m up::Send, {p Up}


+w::Send, {L DownTemp}
w::
{
	if GetKeyState("CapsLock", "T")
		Send, {L DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {8 DownTemp}
	else
		Send, {l DownTemp}
	Return
}
w up::Send, {l Up}


+v::Send, {F DownTemp}
v::
{
	if GetKeyState("CapsLock", "T")
		Send, {F DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {7 DownTemp}
	else
		Send, {f DownTemp}
	Return
}
v up::Send, {f Up}


+z::Send, {V DownTemp}
z::
{
	if GetKeyState("CapsLock", "T")
		Send, {V DownTemp}
	else if GetKeyState("ScrollLock", "T")
		Send, {6 DownTemp}
	else
		Send, {v DownTemp}
	Return
}
z up::Send, {v Up}



#UseHook off

;
