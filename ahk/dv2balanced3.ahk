; Balanced layout
; convert dvorak to the balanced layout
; see http://shenafu.com/smf/index.php?topic=89.msg497#msg497

;UNSHIFTED
;',.cq xyugj
;roitf mneas
;"wldv khpbz

;SHIFTED
;:()CQ XYUGJ
;ROITF MNEAS
;;WLDV KHPBZ

#SingleInstance force
#InstallKeybdHook
#UseHook on

;; Top row
;'::Send, '
;,::Send, ,
;.::Send, .
p::Send, c
y::Send, q
f::Send, x
g::Send, y
c::Send, u
r::Send, g
l::Send, j

+'::Send, :
+,::Send, (
+.::Send, )
+p::Send, C
+y::Send, Q
+f::Send, X
+g::Send, Y
+c::Send, U
+r::Send, G
+l::Send, J

;; Home row
a::Send, r
o::Send, o
e::Send, i
u::Send, t
i::Send, f
d::Send, m
h::Send, n
t::Send, e
n::Send, a
s::Send, s

+a::Send, R
+o::Send, O
+e::Send, I
+u::Send, T
+i::Send, F
+d::Send, M
+h::Send, N
+t::Send, E
+n::Send, A
+s::Send, S

;; Bottom row
SC02C::Send, "
q::Send, w
j::Send, l
k::Send, d
x::Send, v
b::Send, k
m::Send, h
w::Send, p
v::Send, b
z::Send, z

+SC02C::Send, {;} ; semicolon
+q::Send, W
+j::Send, L
+k::Send, D
+x::Send, V
+b::Send, K
+m::Send, H
+w::Send, P
+v::Send, B
+z::Send, Z

;; Number row
;`::Send, 7
;1::Send, 9
;2::Send, 5
;3::Send, 1
;4::Send, 3
;5::Send, =
;6::Send, /
;7::Send, 2
;8::Send, 0
;9::Send, 4
;0::Send, 6
;[::Send, 8
;]::Send, \

;+`::Send, {SC029} ; backquote
;+1::Send, +2
;+2::Send, +[
;+3::Send, +/
;+4::Send, +5
;+5::Send, {+}
;+6::Send, +8
;+7::Send, +7
;+8::Send, +1
+9::Send, <
+0::Send, >
;+[::Send, +6
;+]::Send, +4

#UseHook off

;
