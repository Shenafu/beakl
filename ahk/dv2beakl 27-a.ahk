#include dv2beakl init.ahk

;L0
; qhoux gcmnz
; yiea. dstrb ;
; j",k' wflpv

;S0
; QHOUX GCMNZ
; YIEA` DSTRB ;
; J!?K' WFLPV

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Variables

; Top Row
LAYOUT_KEYS["'"]["l0"] := "q"
LAYOUT_KEYS["+'"]["l0"] := "Q"
LAYOUT_KEYS[","]["l0"] := "h"
LAYOUT_KEYS["+,"]["l0"] := "H"
LAYOUT_KEYS["."]["l0"] := "o"
LAYOUT_KEYS["+."]["l0"] := "O"
LAYOUT_KEYS["p"]["l0"] := "u"
LAYOUT_KEYS["+p"]["l0"] := "U"
LAYOUT_KEYS["y"]["l0"] := "x"
LAYOUT_KEYS["+y"]["l0"] := "X"
LAYOUT_KEYS["f"]["l0"] := "g"
LAYOUT_KEYS["+f"]["l0"] := "G"
LAYOUT_KEYS["g"]["l0"] := "c"
LAYOUT_KEYS["+g"]["l0"] := "C"
LAYOUT_KEYS["c"]["l0"] := "m"
LAYOUT_KEYS["+C"]["l0"] := "M"
LAYOUT_KEYS["r"]["l0"] := "n"
LAYOUT_KEYS["+r"]["l0"] := "N"
LAYOUT_KEYS["l"]["l0"] := "z"
LAYOUT_KEYS["+L"]["l0"] := "Z"

; Home Row
LAYOUT_KEYS["a"]["l0"] := "y"
LAYOUT_KEYS["+a"]["l0"] := "Y"
LAYOUT_KEYS["o"]["l0"] := "i"
LAYOUT_KEYS["+o"]["l0"] := "I"
LAYOUT_KEYS["e"]["l0"] := "e"
LAYOUT_KEYS["+e"]["l0"] := "E"
LAYOUT_KEYS["u"]["l0"] := "a"
LAYOUT_KEYS["+u"]["l0"] := "A"
LAYOUT_KEYS["i"]["l0"] := "."
LAYOUT_KEYS["+i"]["l0"] := "``"
LAYOUT_KEYS["d"]["l0"] := "d"
LAYOUT_KEYS["+d"]["l0"] := "D"
LAYOUT_KEYS["h"]["l0"] := "s"
LAYOUT_KEYS["+h"]["l0"] := "S"
LAYOUT_KEYS["t"]["l0"] := "t"
LAYOUT_KEYS["+t"]["l0"] := "T"
LAYOUT_KEYS["n"]["l0"] := "r"
LAYOUT_KEYS["+n"]["l0"] := "R"
LAYOUT_KEYS["s"]["l0"] := "b"
LAYOUT_KEYS["+s"]["l0"] := "B"
LAYOUT_KEYS["\"]["l0"] := ";"
LAYOUT_KEYS["+\"]["l0"] := ";"

; Bottom Row
LAYOUT_KEYS[";"]["l0"] := "j"
LAYOUT_KEYS["+;"]["l0"] := "J"
LAYOUT_KEYS["q"]["l0"] := """"
LAYOUT_KEYS["+q"]["l0"] := "{!}"
LAYOUT_KEYS["j"]["l0"] := ","
LAYOUT_KEYS["+j"]["l0"] := "?"
LAYOUT_KEYS["k"]["l0"] := "k"
LAYOUT_KEYS["+k"]["l0"] := "K"
LAYOUT_KEYS["x"]["l0"] := "'"
LAYOUT_KEYS["+x"]["l0"] := "'"
LAYOUT_KEYS["b"]["l0"] := "w"
LAYOUT_KEYS["+b"]["l0"] := "W"
LAYOUT_KEYS["m"]["l0"] := "f"
LAYOUT_KEYS["+m"]["l0"] := "F"
LAYOUT_KEYS["w"]["l0"] := "l"
LAYOUT_KEYS["+w"]["l0"] := "L"
LAYOUT_KEYS["v"]["l0"] := "p"
LAYOUT_KEYS["+v"]["l0"] := "P"
LAYOUT_KEYS["z"]["l0"] := "v"
LAYOUT_KEYS["+z"]["l0"] := "V"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BEAKL_BeginReplace()
