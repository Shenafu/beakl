; Balanced Effortless Advanced Keyboard Layout
; convert dvorak to the BEAKL Opted layout

;UNSHIFTED
;znmcg .oiuj
;prtsd ,aehk
;vlwfb q."yx

;SHIFTED
;ZNMCG ;OIUJ
;PRTSD :AEHK
;VLWFB Q()YX

#SingleInstance force
#InstallKeybdHook
#UseHook on

;; Top row
'::Send, z
,::Send, n
.::Send, m
p::Send, c
y::Send, g
f::Send, '
g::Send, o
c::Send, i
r::Send, u
l::Send, j

+'::Send, Z
+,::Send, N
+.::Send, M
+p::Send, C
+y::Send, G
+f::Send, {;}
+g::Send, O
+c::Send, I
+r::Send, U
+l::Send, J

;; Home row
a::Send, p
o::Send, r
e::Send, t
u::Send, s
i::Send, d
d::Send, ,
h::Send, a
t::Send, e
n::Send, h
s::Send, k

+a::Send, P
+o::Send, R
+e::Send, T
+u::Send, S
+i::Send, D
+d::Send, :
+h::Send, A
+t::Send, E
+n::Send, H
+s::Send, K

;; Bottom row
SC02C::Send, v
q::Send, l
j::Send, w
k::Send, f
x::Send, b
b::Send, q
m::Send, .
w::Send, "
v::Send, y
z::Send, x

+SC02C::Send, V
+q::Send, L
+j::Send, W
+k::Send, F
+x::Send, B
+b::Send, Q
+m::Send, {(}
+w::Send, {)}
+v::Send, Y
+z::Send, X

;; Number row
;`::Send, 7
;1::Send, 9
;2::Send, 5
;3::Send, 1
;4::Send, 3
;5::Send, =
;6::Send, /
;7::Send, 2
;8::Send, 0
;9::Send, 4
;0::Send, 6
;[::Send, 8
;]::Send, \

;+`::Send, {SC029} ; backquote
;+1::Send, +2
;+2::Send, +[
;+3::Send, +/
;+4::Send, +5
;+5::Send, {+}
;+6::Send, +8
;+7::Send, +7
;+8::Send, +1
+9::Send, <
+0::Send, >
;+[::Send, +6
;+]::Send, +4

#UseHook off

;
