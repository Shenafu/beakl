; Balanced Effortless Advanced Keyboard Layout
; convert dvorak to the BEAKL Opted layout

;UNSHIFTED
; 32104 95678
; qkiu;   gdnfv
; yoea.  mtrsh
; /'-,z  wclpb

;SHIFTED
; QKIUX   GDNFV
; YOEA@  MTRSH
; J`?!Z  WCLPB

;ALTGR / SCROLL LOCK
;  +=*   ^%~
;  <$>x   [_]
; \(")# %{=}| ;
;  j:*+   &^~

;NUM LOCK
;   +=*
; -523:
; 7.104
;  /698,

#SingleInstance force
;#InstallKeybdHook
#UseHook on


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Number row

1::Send, 3

+2::Send, {+}
2::
{
	if GetKeyState("CapsLock", "T")
		Send, {+ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {+ DownTemp}
	else
		Send, {2 DownTemp}
	Return
}
2 up::Send, {2 Up}


+3::Send, {=}
3::
{
	if GetKeyState("CapsLock", "T")
		Send, {= DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {= DownTemp}
	else
		Send, {1 DownTemp}
	Return
}
3 up::Send, {1 Up}


+4::Send, {*}
4::
{
	if GetKeyState("CapsLock", "T")
		Send, {* DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {* DownTemp}
	else
		Send, {0 DownTemp}
	Return
}
4 up::Send, {0 Up}


5::Send, 4
6::Send, 9


+7::Send, {^}
7::
{
	if GetKeyState("CapsLock", "T")
		Send, {^ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {^ DownTemp}
	else
		Send, {5 DownTemp}
	Return
}
7 up::Send, {5 Up}


+8::Send, {`%}
8::
{
	if GetKeyState("CapsLock", "T")
		Send, {`% DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {`% DownTemp}
	else
		Send, {6 DownTemp}
	Return
}
8 up::Send, {6 Up}


+9::Send, {~}
9::
{
	if GetKeyState("CapsLock", "T")
		Send, {~ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {~ DownTemp}
	else
		Send, {7 DownTemp}
	Return
}
9 up::Send, {7 Up}


0::Send, 8


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Top row

+'::Send, {Q DownTemp}
'::
{
	if GetKeyState("CapsLock", "T")
		Send, {- DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {- DownTemp}
	else
		Send, {q DownTemp}
	Return
}
' up::Send, {q Up}


+,::Send, {K DownTemp}
,::
{
	if GetKeyState("CapsLock", "T")
		Send, {< DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {5 DownTemp}
	else
		Send, {k DownTemp}
	Return
}
, up::Send, {k Up}


+.::Send, {I DownTemp}
.::
{
	if GetKeyState("CapsLock", "T")
		Send, {$ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {2 DownTemp}
	else
		Send, {i DownTemp}
	Return
}
. up::Send, {i Up}


+p::Send, {U DownTemp}
p::
{
	if GetKeyState("CapsLock", "T")
		Send, {> DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {3 DownTemp}
	else
		Send, {u DownTemp}
	Return
}
p up::Send, {u Up}


+y::Send, {X DownTemp}
y::
{
	if GetKeyState("CapsLock", "T")
		Send, {x DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {: DownTemp}
	else
		Send, {; DownTemp}
	Return
}
y up::Send, {; Up}


f::g


+g::Send, {D DownTemp}
g::
{
	if GetKeyState("CapsLock", "T")
		Send, {[ DownTemp}
	else
		Send, {d DownTemp}
	Return
}
g up::Send, {d Up}


+c::Send, {N DownTemp}
c::
{
	if GetKeyState("CapsLock", "T")
		Send, {_ DownTemp}
	else
		Send, {n DownTemp}
	Return
}
c up::Send, {n Up}


+r::Send, {F DownTemp}
r::
{
	if GetKeyState("CapsLock", "T")
		Send, {] DownTemp}
	else
		Send, {f DownTemp}
	Return
}
r up::Send, {f Up}


l::v


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Home row

`::Send, {-}

+a::Send, {Y DownTemp}
a::
{
	if GetKeyState("CapsLock", "T")
		Send, {\ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {7 DownTemp}
	else
		Send, {y DownTemp}
	Return
}
a up::Send, {y Up}


+o::Send, {O DownTemp}
o::
{
	if GetKeyState("CapsLock", "T")
		Send, {( DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {. DownTemp}
	else
		Send, {o DownTemp}
	Return
}
o up::Send, {o Up}


+e::Send, {E DownTemp}
e::
{
	if GetKeyState("CapsLock", "T")
		Send, {" DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {1 DownTemp}
	else
		Send, {e DownTemp}
	Return
}
e up::Send, {e Up}


+u::Send, {A DownTemp}
u::
{
	if GetKeyState("CapsLock", "T")
		Send, {) DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {0 DownTemp}
	else
		Send, {a DownTemp}
	Return
}
u up::Send, {a Up}


+i::Send, {@ DownTemp}
i::
{
	if GetKeyState("CapsLock", "T")
		Send, {# DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {4 DownTemp}
	else
		Send, {. DownTemp}
	Return
}
i up::Send, {. Up}


+d::Send, {M DownTemp}
d::
{
	if GetKeyState("CapsLock", "T")
		Send, {`% DownTemp}
	else
		Send, {m DownTemp}
	Return
}
d up::Send, {m Up}


+h::Send, {T DownTemp}
h::
{
	if GetKeyState("CapsLock", "T")
		Send, {`{ DownTemp}
	else
		Send, {t DownTemp}
	Return
}
h up::Send, {t Up}


+t::Send, {R DownTemp}
t::
{
	if GetKeyState("CapsLock", "T")
		Send, {= DownTemp}
	else
		Send, {r DownTemp}
	Return
}
t up::Send, {r Up}


+n::Send, {S DownTemp}
n::
{
	if GetKeyState("CapsLock", "T")
		Send, {`} DownTemp}
	else
		Send, {s DownTemp}
	Return
}
n up::Send, {s Up}


+s::Send, {H DownTemp}
s::
{
	if GetKeyState("CapsLock", "T")
		Send, {| DownTemp}
	else
		Send, {h DownTemp}
	Return
}
s up::Send, {h Up}


\::Send, {;}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bottom row

+SC02C::Send, {J}
SC02C::
{
	if GetKeyState("CapsLock", "T")
		Send, {j DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {/ DownTemp}
	else
		Send, {/ DownTemp}
	Return
}
SC02C up::Send, {/ Up}


+q::Send, {`` DownTemp}
q::
{
	if GetKeyState("CapsLock", "T")
		Send, {: DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {6 DownTemp}
	else
		Send, {' DownTemp}
	Return
}
q up::Send, {' Up}


+j::Send, {? DownTemp}
j::
{
	if GetKeyState("CapsLock", "T")
		Send, {* DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {9 DownTemp}
	else
		Send, {- DownTemp}
	Return
}
j up::Send, {- Up}


+k::Send, {! DownTemp}
k::
{
	if GetKeyState("CapsLock", "T")
		Send, {+ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {8 DownTemp}
	else
		Send, {, DownTemp}
	Return
}
k up::Send, {, Up}


+x::Send, {Z DownTemp}
x::
{
	if GetKeyState("NumLock", "T")
		Send, {, DownTemp}
	else
		Send, {z DownTemp}
	Return
}
x up::Send, {z Up}


+b::Send, {W DownTemp}
b::
{
	if GetKeyState("CapsLock", "T")
		Send, {W DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {& DownTemp}
	else
		Send, {w DownTemp}
	Return
}
b up::Send, {w Up}


+m::Send, {C DownTemp}
m::
{
	if GetKeyState("CapsLock", "T")
		Send, {& DownTemp}
	else
		Send, {c DownTemp}
	Return
}
m up::Send, {c Up}


+w::Send, {L DownTemp}
w::
{
	if GetKeyState("CapsLock", "T")
		Send, {^ DownTemp}
	else
		Send, {l DownTemp}
	Return
}
w up::Send, {l Up}


+v::Send, {P DownTemp}
v::
{
	if GetKeyState("CapsLock", "T")
		Send, {~ DownTemp}
	else
		Send, {p DownTemp}
	Return
}
v up::Send, {p Up}


z::b



#UseHook off

;
