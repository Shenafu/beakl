#include dv2beakl init.ahk

;L0
; juioq gcrfz
; yhea, dstnb ;
; xk".' wmlpv

;S0
; JUIOQ GCRFZ
; YHEA` DSTNB ;
; XK!?' WMLPV

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Variables

; Top Row
LAYOUT_KEYS["'"]["l0"] := "j"
LAYOUT_KEYS["+'"]["l0"] := "J"
LAYOUT_KEYS[","]["l0"] := "u"
LAYOUT_KEYS["+,"]["l0"] := "U"
LAYOUT_KEYS["."]["l0"] := "i"
LAYOUT_KEYS["+."]["l0"] := "I"
LAYOUT_KEYS["p"]["l0"] := "o"
LAYOUT_KEYS["+p"]["l0"] := "O"
LAYOUT_KEYS["y"]["l0"] := "q"
LAYOUT_KEYS["+y"]["l0"] := "Q"
LAYOUT_KEYS["f"]["l0"] := "g"
LAYOUT_KEYS["+f"]["l0"] := "G"
LAYOUT_KEYS["g"]["l0"] := "c"
LAYOUT_KEYS["+g"]["l0"] := "C"
LAYOUT_KEYS["c"]["l0"] := "r"
LAYOUT_KEYS["+C"]["l0"] := "R"
LAYOUT_KEYS["r"]["l0"] := "f"
LAYOUT_KEYS["+r"]["l0"] := "F"
LAYOUT_KEYS["l"]["l0"] := "z"
LAYOUT_KEYS["+L"]["l0"] := "Z"

; Home Row
LAYOUT_KEYS["a"]["l0"] := "y"
LAYOUT_KEYS["+a"]["l0"] := "Y"
LAYOUT_KEYS["o"]["l0"] := "h"
LAYOUT_KEYS["+o"]["l0"] := "H"
LAYOUT_KEYS["e"]["l0"] := "e"
LAYOUT_KEYS["+e"]["l0"] := "E"
LAYOUT_KEYS["u"]["l0"] := "a"
LAYOUT_KEYS["+u"]["l0"] := "A"
LAYOUT_KEYS["i"]["l0"] := ","
LAYOUT_KEYS["+i"]["l0"] := "``"
LAYOUT_KEYS["d"]["l0"] := "d"
LAYOUT_KEYS["+d"]["l0"] := "D"
LAYOUT_KEYS["h"]["l0"] := "s"
LAYOUT_KEYS["+h"]["l0"] := "S"
LAYOUT_KEYS["t"]["l0"] := "t"
LAYOUT_KEYS["+t"]["l0"] := "T"
LAYOUT_KEYS["n"]["l0"] := "n"
LAYOUT_KEYS["+n"]["l0"] := "N"
LAYOUT_KEYS["s"]["l0"] := "b"
LAYOUT_KEYS["+s"]["l0"] := "B"
LAYOUT_KEYS["\"]["l0"] := ";"
LAYOUT_KEYS["+\"]["l0"] := ";"

; Bottom Row
LAYOUT_KEYS[";"]["l0"] := "x"
LAYOUT_KEYS["+;"]["l0"] := "X"
LAYOUT_KEYS["q"]["l0"] := "k"
LAYOUT_KEYS["+q"]["l0"] := "K"
LAYOUT_KEYS["j"]["l0"] := """"
LAYOUT_KEYS["+j"]["l0"] := "{!}"
LAYOUT_KEYS["k"]["l0"] := "."
LAYOUT_KEYS["+k"]["l0"] := "?"
LAYOUT_KEYS["x"]["l0"] := "'"
LAYOUT_KEYS["+x"]["l0"] := "'"
LAYOUT_KEYS["b"]["l0"] := "w"
LAYOUT_KEYS["+b"]["l0"] := "W"
LAYOUT_KEYS["m"]["l0"] := "m"
LAYOUT_KEYS["+m"]["l0"] := "M"
LAYOUT_KEYS["w"]["l0"] := "l"
LAYOUT_KEYS["+w"]["l0"] := "L"
LAYOUT_KEYS["v"]["l0"] := "p"
LAYOUT_KEYS["+v"]["l0"] := "P"
LAYOUT_KEYS["z"]["l0"] := "v"
LAYOUT_KEYS["+z"]["l0"] := "V"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BEAKL_BeginReplace()
