; Balanced Effortless Advanced Keyboard Layout
; convert dvorak to the BEAKL3 layout

;UNSHIFTED
;'youf mgds"
;kaeip bhtnw
;v,qlx jrc.z

;SHIFTED
;;YOUF MGDS:
;KAEIP BHTNW
;V(QLX JRC)Z

#SingleInstance force
#InstallKeybdHook
#UseHook on

;; Top row
'::Send, '
,::Send, y
.::Send, o
p::Send, u
y::Send, f
f::Send, m
g::Send, g
c::Send, d
r::Send, s
l::Send, "

+'::Send, {;}
+,::Send, Y
+.::Send, O
+p::Send, U
+y::Send, F
+f::Send, M
+g::Send, G
+c::Send, D
+r::Send, S
+l::Send, :

;; Home row
a::Send, k
o::Send, a
e::Send, e
u::Send, i
i::Send, p
d::Send, b
h::Send, h
t::Send, t
n::Send, n
s::Send, w

+a::Send, K
+o::Send, A
+e::Send, E
+u::Send, I
+i::Send, P
+d::Send, B
+h::Send, H
+t::Send, T
+n::Send, N
+s::Send, W

;; Bottom row
SC02C::Send, v
q::Send, ,
j::Send, q
k::Send, l
x::Send, x
b::Send, j
m::Send, r
w::Send, c
v::Send, .
z::Send, z

+SC02C::Send, V
+q::Send, {(}
+j::Send, Q
+k::Send, L
+x::Send, X
+b::Send, J
+m::Send, R
+w::Send, C
+v::Send, {)}
+z::Send, Z

;; Number row
;`::Send, 7
;1::Send, 9
;2::Send, 5
;3::Send, 1
;4::Send, 3
;5::Send, =
;6::Send, /
;7::Send, 2
;8::Send, 0
;9::Send, 4
;0::Send, 6
;[::Send, 8
;]::Send, \

;+`::Send, {SC029} ; backquote
;+1::Send, +2
;+2::Send, +[
;+3::Send, +/
;+4::Send, +5
;+5::Send, {+}
;+6::Send, +8
;+7::Send, +7
;+8::Send, +1
+9::Send, <
+0::Send, >
;+[::Send, +6
;+]::Send, +4

#UseHook off

;
