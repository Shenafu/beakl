#SingleInstance force
;#InstallKeybdHook
#UseHook on

; http://shenafu.com/smf/index.php?topic=186.msg2719#msg2719

;Punc
;         q
;        ["]
;       ;(')z
;        {x}
;
;Num
;         =
;        32*
;       /01,+
;        -4.
;
;L1
;         j
;        gym
;       kpcdw
;        bvf
;
;L0
;         u
;        arn
;       oe ts
;        ihl
;
;SP
;         Q
;        |_^
;       @\`?Z
;        &X~
;
;SN
;         %
;        87:
;       $56#!
;        <9>
;
;S1
;         J
;        GYM
;       KPCDW
;        BVF
;
;S0
;         U
;        ARN
;       OE TS
;        IHL
;
;
;Fn
;          F1
;       F2 F3 F4
;    F5 F6 F7 F8 F9
;      F10 F11 F12
;
;Sys
;                ScrLk
;         PrSc   Esc     Ins
; NumLk   Copy   Paste   Cut  CapLk
;        Pause   SelAll  Save
;
;Move
;               ScrUp
;         Tab    Up     PgUp
;   Home  Left   Down    Right   End
;         Click  ScrDn  PgDn
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Variables

LAYOUT_LAYER := "l0"

LAYOUT_KEYS := {}
; Upper Row
; SC009 = '8'
LAYOUT_KEYS["SC009"] := { l0: "u", l1: "j", num: "=", punc: "q", move: "{WheelUp}", fn: "{F1}", sys: "{ScrollLock}" }
LAYOUT_KEYS["+SC009"] := { l0: "U", l1: "J", num: "%", punc: "Q", move: "+{WheelUp}", fn: "+{F1}", sys: "+{ScrollLock}" }

; Top Row
LAYOUT_KEYS["g"] := { l0: "a", l1: "g", num: "3", punc: "[", move: "{tab}", fn: "{F2}", sys: "{PrintScreen}" }
LAYOUT_KEYS["+g"] := { l0: "A", l1: "G", num: "8", punc: "|", move: "+{tab}", fn: "+{F2}", sys: "+{PrintScreen}" }
LAYOUT_KEYS["c"] := { l0: "r", l1: "y", num: "2", punc: """", move: "{up}", fn: "{F3}", sys: "{Escape}" }
LAYOUT_KEYS["+c"] := { l0: "R", l1: "Y", num: "7", punc: "_", move: "+{up}", fn: "+{F3}", sys: "+{Escape}" }
LAYOUT_KEYS["r"] := { l0: "n", l1: "m", num: ",", punc: "]", move: "{PgUp}", fn: "{F4}", sys: "{Insert}" }
LAYOUT_KEYS["+r"] := { l0: "N", l1: "M", num: ":", punc: "{^}", move: "+{PgUp}", fn: "+{F4}", sys: "+{Insert}" }

; Home Row
LAYOUT_KEYS["d"] := { l0: "o", l1: "k", num: "/", punc: "{;}", move: "{Home}", fn: "{F5}", sys: "{NumLock}" }
LAYOUT_KEYS["+d"] := { l0: "O", l1: "K", num: "$", punc: "@", move: "+{Home}", fn: "+{F5}", sys: "+{NumLock}" }
LAYOUT_KEYS["h"] := { l0: "e", l1: "p", num: "{0}", punc: "(", move: "{Left}", fn: "{F6}", sys: "^c" }
LAYOUT_KEYS["+h"] := { l0: "E", l1: "P", num: "5", punc: "\", move: "+{Left}", fn: "+{F6}", sys: "^+c" }
LAYOUT_KEYS["t"] := { l0: "{space}", l1: "c", num: "1", punc: "'", move: "{Down}", fn: "{F7}", sys: "^v" }
LAYOUT_KEYS["+t"] := { l0: "{enter}", l1: "C", num: "6", punc: "``", move: "+{Down}", fn: "+{F7}", sys: "^+v" }
LAYOUT_KEYS["n"] := { l0: "t", l1: "d", num: ".", punc: ")", move: "{Right}", fn: "{F8}", sys: "^x" }
LAYOUT_KEYS["+n"] := { l0: "T", l1: "D", num: "{#}", punc: "?", move: "+{Right}", fn: "+{F8}", sys: "^+x" }
LAYOUT_KEYS["s"] := { l0: "s", l1: "w", num: "{+}", punc: "z", move: "{End}", fn: "{F9}", sys: "{CapsLock}" }
LAYOUT_KEYS["+s"] := { l0: "S", l1: "W", num: "{!}", punc: "Z", move: "+{End}", fn: "+{F9}", sys: "+{CapsLock}" }

; Bottom Row
LAYOUT_KEYS["m"] := { l0: "i", l1: "b", num: "-", punc: "{{}", move: "{LButton}", fn: "{F10}", sys: "{Pause}" }
LAYOUT_KEYS["+m"] := { l0: "I", l1: "B", num: "<", punc: "&", move: "{RButton}", fn: "+{F10}", sys: "+{Pause}" }
LAYOUT_KEYS["w"] := { l0: "h", l1: "v", num: "4", punc: "x", move: "{WheelDown}", fn: "{F11}", sys: "^a" }
LAYOUT_KEYS["+w"] := { l0: "H", l1: "V", num: "9", punc: "X", move: "+{WheelDown}", fn: "+{F11}", sys: "^+a" }
LAYOUT_KEYS["v"] := { l0: "l", l1: "f", num: "*", punc: "{}}", move: "{PgDn}", fn: "{F12}", sys: "^s" }
LAYOUT_KEYS["+v"] := { l0: "L", l1: "F", num: ">", punc: "~", move: "+{PgDn}", fn: "+{F12}", sys: "^+s" }

LAYOUT_TOGGLERS := { capslock: "l0", z: "l1", b: "punc", f: "num", 6: "sys", 7: "move", 9: "fn" }

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; FUNCTIONS

BEAKL_RunKey(key) {
	global LAYOUT_LAYER, LAYOUT_KEYS

	If % (LAYOUT_KEYS[key][LAYOUT_LAYER]) {
		Send % LAYOUT_KEYS[key][LAYOUT_LAYER]
	}
	else {
		Send % key
	}
}

BEAKL_ToggleLayer(k) {
	global LAYOUT_LAYER, LAYOUT_TOGGLERS
	layer := LAYOUT_TOGGLERS[k]

	If (LAYOUT_LAYER = layer) {
		LAYOUT_LAYER := "l0"
	}
	else
		LAYOUT_LAYER := layer
}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;; Input replacements

; Toggle layers

For k, v in LAYOUT_TOGGLERS {
	Hotkey %k%, LABEL_ToggleLayer
}

; Main Chars

For k, v in LAYOUT_KEYS {
	Hotkey %k%, LABEL_RunKey
}

; Backspace

l::send, {backspace}
+l::send, {delete}

LABEL_RunKey:
	k := A_ThisHotkey
	BEAKL_RunKey(k)
	return

LABEL_ToggleLayer:
	k := A_ThisHotkey
	BEAKL_ToggleLayer(k)
	return

