; Balanced Effortless Advanced Keyboard Layout
; convert dvorak to the BEAKL Opted layout

;UNSHIFTED
;znlcf 'uohq
;prtsd .aeik
;vbmgw x,"yj

;SHIFTED
;ZNLCF ;UOHQ
;PRTSD :AEIK
;VBMGW X()YJ

#SingleInstance force
#InstallKeybdHook
#UseHook on

;; Top row
'::Send, z
,::Send, n
.::Send, l
p::Send, c
y::Send, f
f::Send, '
g::Send, u
c::Send, o
r::Send, h
l::Send, q

+'::Send, Z
+,::Send, N
+.::Send, L
+p::Send, C
+y::Send, F
+f::Send, {;}
+g::Send, U
+c::Send, O
+r::Send, H
+l::Send, Q

;; Home row
a::Send, p
o::Send, r
e::Send, t
u::Send, s
i::Send, d
d::Send, .
h::Send, a
t::Send, e
n::Send, i
s::Send, k

+a::Send, P
+o::Send, R
+e::Send, T
+u::Send, S
+i::Send, D
+d::Send, :
+h::Send, A
+t::Send, E
+n::Send, I
+s::Send, K

;; Bottom row
SC02C::Send, v
q::Send, b
j::Send, m
k::Send, g
x::Send, w
b::Send, x
m::Send, ,
w::Send, "
v::Send, y
z::Send, j

+SC02C::Send, V
+q::Send, B
+j::Send, M
+k::Send, G
+x::Send, W
+b::Send, X
+m::Send, {(}
+w::Send, {)}
+v::Send, Y
+z::Send, J

;; Number row
;`::Send, 7
;1::Send, 9
;2::Send, 5
;3::Send, 1
;4::Send, 3
;5::Send, =
;6::Send, /
;7::Send, 2
;8::Send, 0
;9::Send, 4
;0::Send, 6
;[::Send, 8
;]::Send, \

;+`::Send, {SC029} ; backquote
;+1::Send, +2
;+2::Send, +[
;+3::Send, +/
;+4::Send, +5
;+5::Send, {+}
;+6::Send, +8
;+7::Send, +7
;+8::Send, +1
+9::Send, <
+0::Send, >
;+[::Send, +6
;+]::Send, +4

#UseHook off

;
