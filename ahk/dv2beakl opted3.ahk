; Balanced Effortless Advanced Keyboard Layout
; convert dvorak to the BEAKL Opted layout

;UNSHIFTED
;jho.x fclnz
;kieau dstrp
;qy",' wgmbv

;SHIFTED
;JHO:X FCLNZ
;KIEAU DSTRP
;QY(); WGMBV

#SingleInstance force
#InstallKeybdHook
#UseHook on

;; Top row
'::Send, j
,::Send, h
.::Send, o
p::Send, .
y::Send, x
f::Send, f
g::Send, c
c::Send, l
r::Send, n
l::Send, z

+'::Send, J
+,::Send, H
+.::Send, O
+p::Send, :
+y::Send, X
+f::Send, F
+g::Send, C
+c::Send, L
+r::Send, N
+l::Send, Z

;; Home row
a::Send, k
o::Send, i
e::Send, e
u::Send, a
i::Send, u
d::Send, d
h::Send, s
t::Send, t
n::Send, r
s::Send, p

+a::Send, K
+o::Send, I
+e::Send, E
+u::Send, A
+i::Send, U
+d::Send, D
+h::Send, S
+t::Send, T
+n::Send, R
+s::Send, P

;; Bottom row
SC02C::Send, q
q::Send, y
j::Send, "
k::Send, ,
x::Send, '
b::Send, w
m::Send, g
w::Send, m
v::Send, b
z::Send, v

+SC02C::Send, Q
+q::Send, Y
+j::Send, {(}
+k::Send, {)}
+x::Send, {;}
+b::Send, W
+m::Send, G
+w::Send, M
+v::Send, B
+z::Send, V

;; Number row
;`::Send, =
1::Send, 9
2::Send, 3
3::Send, 1
4::Send, 5
5::Send, 7
6::Send, 6
7::Send, 4
8::Send, 0
9::Send, 2
0::Send, 8
;[::Send, /
;]::Send, \

;+`::Send, {SC029} ; backquote
;+1::Send, +2
;+2::Send, +[
;+3::Send, +/
;+4::Send, +5
;+5::Send, {+}
;+6::Send, +8
;+7::Send, +7
;+8::Send, +1
+9::Send, <
+0::Send, >
;+[::Send, +6
;+]::Send, +4

#UseHook off

;
