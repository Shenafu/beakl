; Balanced Effortless Advanced Keyboard Layout
; convert dvorak to the BEAKL Opted layout

;UNSHIFTED
;  40123 87659
;- xyou' wdlnv z
; hiea. gstrp
;j ("),: fcmbk q

;SHIFTED
;  \&#$? |@*%^
;` XYOU! WDLNV Z
; HIEA_ GSTRP
;J <;>/~ FCMBK Q

#SingleInstance force
;#InstallKeybdHook
#UseHook on

;; Top row
`::Send, -

'::Send, x

,::
{
if not GetKeyState("Numlock", "T")
	Send, {y DownTemp}
else
	Send, {2 DownTemp}
Return
}
, up::Send, {y Up}

.::
{
if not GetKeyState("Numlock", "T")
	Send, {o DownTemp}
else
	Send, {9 DownTemp}
Return
}
. up::Send, {o Up}

p::
{
if not GetKeyState("Numlock", "T")
	Send, {u DownTemp}
else
	Send, {7 DownTemp}
Return
}
p up::Send, {u Up}

y::
{
if not GetKeyState("Numlock", "T")
	Send, {' DownTemp}
else
	Send, {6 DownTemp}
Return
}
y up::Send, {' Up}

f::Send, w
g::Send, d
c::Send, l
r::Send, n
l::Send, v

\::Send, z

~::Send, ``

+'::Send, X
+,::Send, Y
+.::Send, O
+p::Send, U
+y::Send, {!}
+f::Send, W
+g::Send, D
+c::Send, L
+r::Send, N
+l::Send, V

|::Send, Z

;; Home row
a::
{
if not GetKeyState("Numlock", "T")
	Send, {h DownTemp}
else
	Send, {5 DownTemp}
Return
}
a up::Send, {h Up}

o::
{
if not GetKeyState("Numlock", "T")
	Send, {i DownTemp}
else
	Send, {1 DownTemp}
Return
}
o up::Send, {i Up}

e::
{
if not GetKeyState("Numlock", "T")
	Send, {e DownTemp}
else
	Send, {0 DownTemp}
Return
}
e up::Send, {e Up}

u::
{
if not GetKeyState("Numlock", "T")
	Send, {a DownTemp}
else
	Send, {3 DownTemp}
Return
}
u up::Send, {a Up}

i::Send, {. DownTemp}
i up::Send, {. Up}

d::Send, g
h::Send, s
t::Send, t
n::Send, r
s::Send, p

+a::Send, H
+o::Send, I
+e::Send, E
+u::Send, A
+i::Send, _
+d::Send, G
+h::Send, S
+t::Send, T
+n::Send, R
+s::Send, P

;; Bottom row
/::Send, j

SC02C::
{
if not GetKeyState("Numlock", "T")
	Send, {( DownTemp}
else
	Send, {- DownTemp}
Return
}
SC02C up::Send, {( up}

q::
if not GetKeyState("Numlock", "T")
	Send, {" DownTemp}
else
	Send, {4 DownTemp}
Return
q up::Send, {" up}

j::
{
if not GetKeyState("Numlock", "T")
	Send, {) DownTemp}
else
	Send, {8 DownTemp}
Return
}
j up::Send, {) up}

k::
{
if not GetKeyState("Numlock", "T")
	Send, {, DownTemp}
else
	Send, {/ DownTemp}
Return
}
k up::Send, {, up}

x::Send, :
b::Send, f
m::Send, c
w::Send, m
v::Send, b
z::Send, k

-::Send, q

+/::Send, J

+SC02C::Send, <
+q::Send, {;}
+j::Send, >
+k::Send, /
+x::Send, ~
+b::Send, F
+m::Send, C
+w::Send, M
+v::Send, B
+z::Send, K

+-::Send, Q

;; Number row
1::Send, 4
2::Send, 0
3::Send, 1
4::Send, 2
5::Send, 3
6::Send, 8
7::Send, 7
8::Send, 6
9::Send, 5
0::Send, 9

+1::Send, \
+2::Send, &
+3::Send, {#}
+4::Send, $
+5::Send, ?
+6::Send, |
+7::Send, @
+8::Send, *
+9::Send, `%
+0::Send, {^}

#UseHook off

;
