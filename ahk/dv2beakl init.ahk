#SingleInstance force
#InstallKeybdHook
#UseHook on

#IfWinActive, ahk_exe PathOfExile.exe
	SetKeyDelay, -1, 50


#IfWinNotActive, ahk_class VMPlayerFrame
#IfWinNotActive, ahk_exe vmplayer.exe
#IfWinNotActive, ahk_exe dosbox.exe


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Variables

LAYOUT_LAYER := "l0"

LAYOUT_KEYS := {}

LAYOUT_TOGGLERS := { Capslock: "punc", numlock: "num", scrollLock: "move" }

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;; FUNCTIONS


BEAKL_RunKey(key) {
	global LAYOUT_LAYER, LAYOUT_KEYS

	If % (LAYOUT_KEYS[key][LAYOUT_LAYER]) {
		Out := % LAYOUT_KEYS[key][LAYOUT_LAYER]
		If % GetKeyState(key) {
			Send {%Out% DownTemp}
		}
		else {
			Send %Out%
		}
	}
	else {
		Send % key
		return
	}
}

BEAKL_ToggleLayer(k) {
	global LAYOUT_LAYER, LAYOUT_TOGGLERS
	layer := LAYOUT_TOGGLERS[k]

	; turn off lights
	SetNumLockState , off
	SetCapsLockState , off
	SetScrollLockState , off

	If (LAYOUT_LAYER = layer) {
		LAYOUT_LAYER := "l0"
	}
	else {
		LAYOUT_LAYER := layer

		; turn on lights

		if (LAYOUT_LAYER = "num") {
			SetNumLockState, on
		}
		else if (LAYOUT_LAYER = "punc") {
			SetCapsLockState, on
		}
		else if (LAYOUT_LAYER = "move") {
			SetScrollLockState, on
		}
	}
}

BEAKL_BeginReplace() {
	global LAYOUT_LAYER, LAYOUT_TOGGLERS, LAYOUT_KEYS

	;;; Input replacements

	; Toggle layers

	For k, v in LAYOUT_TOGGLERS {
		Hotkey, %k%, LABEL_ToggleLayer
	}

	; Main Chars

	For k, v in LAYOUT_KEYS {
		Hotkey, %k%, LABEL_RunKey
	}

	LABEL_RunKey:
		k := A_ThisHotkey
		BEAKL_RunKey(k)
		return

	LABEL_ToggleLayer:
		k := A_ThisHotkey
		BEAKL_ToggleLayer(k)
		return
}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; LAYOUT_KEYS

; SC009 = '8'
LAYOUT_KEYS["SC002"] := {}
LAYOUT_KEYS["+SC002"] := {}
LAYOUT_KEYS["SC003"] := {}
LAYOUT_KEYS["+SC003"] := {}
LAYOUT_KEYS["SC004"] := {}
LAYOUT_KEYS["+SC004"] := {}
LAYOUT_KEYS["SC005"] := {}
LAYOUT_KEYS["+SC005"] := {}
LAYOUT_KEYS["SC006"] := {}
LAYOUT_KEYS["+SC006"] := {}
LAYOUT_KEYS["SC007"] := {}
LAYOUT_KEYS["+SC007"] := {}
LAYOUT_KEYS["SC008"] := {}
LAYOUT_KEYS["+SC008"] := {}
LAYOUT_KEYS["SC009"] := {}
LAYOUT_KEYS["+SC009"] := {}
LAYOUT_KEYS["SC00A"] := {}
LAYOUT_KEYS["+SC00A"] := {}
LAYOUT_KEYS["SC00B"] := {}
LAYOUT_KEYS["+SC00B"] := {}

; Top Row
LAYOUT_KEYS["'"] := {}
LAYOUT_KEYS["+'"] := {}
LAYOUT_KEYS[","] := {}
LAYOUT_KEYS["+,"] := {}
LAYOUT_KEYS["."] := {}
LAYOUT_KEYS["+."] := {}
LAYOUT_KEYS["p"] := {}
LAYOUT_KEYS["+p"] := {}
LAYOUT_KEYS["y"] := {}
LAYOUT_KEYS["+y"] := {}
LAYOUT_KEYS["f"] := {}
LAYOUT_KEYS["+f"] := {}
LAYOUT_KEYS["g"] := {}
LAYOUT_KEYS["+g"] := {}
LAYOUT_KEYS["c"] := {}
LAYOUT_KEYS["+C"] := {}
LAYOUT_KEYS["r"] := {}
LAYOUT_KEYS["+r"] := {}
LAYOUT_KEYS["l"] := {}
LAYOUT_KEYS["+L"] := {}

; Home Row
LAYOUT_KEYS["a"] := {}
LAYOUT_KEYS["+a"] := {}
LAYOUT_KEYS["o"] := {}
LAYOUT_KEYS["+o"] := {}
LAYOUT_KEYS["e"] := {}
LAYOUT_KEYS["+e"] := {}
LAYOUT_KEYS["u"] := {}
LAYOUT_KEYS["+u"] := {}
LAYOUT_KEYS["i"] := {}
LAYOUT_KEYS["+i"] := {}
LAYOUT_KEYS["d"] := {}
LAYOUT_KEYS["+d"] := {}
LAYOUT_KEYS["h"] := {}
LAYOUT_KEYS["+h"] := {}
LAYOUT_KEYS["t"] := {}
LAYOUT_KEYS["+t"] := {}
LAYOUT_KEYS["n"] := {}
LAYOUT_KEYS["+n"] := {}
LAYOUT_KEYS["s"] := {}
LAYOUT_KEYS["+s"] := {}
LAYOUT_KEYS["\"] := {}
LAYOUT_KEYS["+\"] := {}

; Bottom Row
LAYOUT_KEYS[";"] := {}
LAYOUT_KEYS["+;"] := {}
LAYOUT_KEYS["q"] := {}
LAYOUT_KEYS["+q"] := {}
LAYOUT_KEYS["j"] := {}
LAYOUT_KEYS["+j"] := {}
LAYOUT_KEYS["k"] := {}
LAYOUT_KEYS["+k"] := {}
LAYOUT_KEYS["x"] := {}
LAYOUT_KEYS["+x"] := {}
LAYOUT_KEYS["b"] := {}
LAYOUT_KEYS["+b"] := {}
LAYOUT_KEYS["m"] := {}
LAYOUT_KEYS["+m"] := {}
LAYOUT_KEYS["w"] := {}
LAYOUT_KEYS["+w"] := {}
LAYOUT_KEYS["v"] := {}
LAYOUT_KEYS["+v"] := {}
LAYOUT_KEYS["z"] := {}
LAYOUT_KEYS["+z"] := {}


#include dv2beakl punc.ahk
#include dv2beakl numpad.ahk
#include dv2beakl move.ahk

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;; Keys, special

; Backspace

;l::send, {backspace}
;+l::send, {delete}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;