; Balanced Effortless Advanced Keyboard Layout
; convert dvorak to the BEAKL Opted layout

;UNSHIFTED
;jyo.k gcmnz
;hieau dstrp
;q"',x wflbv

;SHIFTED
;JYO:K GCMNZ
;HIEAU DSTRP
;Q(;)X WFLBV

#SingleInstance force
;#InstallKeybdHook
#UseHook on

;; Top row
'::Send, j
,::Send, y
.::Send, o
p::Send, .
y::Send, k
f::Send, g
g::Send, c
c::Send, m
r::Send, n
l::Send, z

+'::Send, J
+,::Send, Y
+.::Send, O
+p::Send, :
+y::Send, K
+f::Send, G
+g::Send, C
+c::Send, M
+r::Send, N
+l::Send, Z

;; Home row
a::Send, h
o::Send, i
e::Send, e
u::Send, a
i::Send, u
d::Send, d
h::Send, s
t::Send, t
n::Send, r
s::Send, p

+a::Send, H
+o::Send, I
+e::Send, E
+u::Send, A
+i::Send, U
+d::Send, D
+h::Send, S
+t::Send, T
+n::Send, R
+s::Send, P

;; Bottom row
SC02C::Send, q
q::Send, "
j::Send, '
k::Send, ,
x::Send, x
b::Send, w
m::Send, f
w::Send, l
v::Send, b
z::Send, v

+SC02C::Send, Q
+q::Send, {(}
+j::Send, {;}
+k::Send, {)}
+x::Send, X
+b::Send, W
+m::Send, F
+w::Send, L
+v::Send, B
+z::Send, V

;; Number row
;`::Send, =
1::Send, 4
2::Send, 0
3::Send, 1
4::Send, 2
5::Send, 3
6::Send, 8
7::Send, 7
8::Send, 6
9::Send, 5
0::Send, 9
;[::Send, /
;]::Send, \

;+`::Send, {SC029} ; backquote
;+1::Send, +2
;+2::Send, +[
;+3::Send, +/
;+4::Send, +5
;+5::Send, {+}
;+6::Send, +8
;+7::Send, +7
;+8::Send, +1
+9::Send, <
+0::Send, >
;+[::Send, +6
;+]::Send, +4

#UseHook off

;
