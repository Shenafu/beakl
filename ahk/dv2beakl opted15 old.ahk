; Balanced Effortless Advanced Keyboard Layout
; convert dvorak to the BEAKL Opted layout

;UNSHIFTED
;  32104 76598
;  qhoux gcrfz
;- yiea. dstnb ;
;  j/,k' wmlpv

;SHIFTED
;  QHOUX GCRFZ
;- YIEA@ DSTNB ;
;  J?!K` WMLPV

;ALTGR / SCROLL LOCK
;   +=*   ^%~
;   <$>   [_]
;- \(")# %{=}| ;
;   :*+   &^~

;NUM LOCK
;  ~+=*
;   523:
;- 7.104
;  /698,

#SingleInstance force
;#InstallKeybdHook
#UseHook on
#MaxHotkeysPerInterval 200

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Number row

+1::Send, {~}
1::Send 3


+2::Send, {+}
2::
{
	if GetKeyState("CapsLock", "T")
		Send, {+ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {+ DownTemp}
	else
		Send, {2 DownTemp}
	Return
}
2 up::Send, {2 Up}


+3::Send, {=}
3::
{
	if GetKeyState("CapsLock", "T")
		Send, {= DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {= DownTemp}
	else
		Send, {1 DownTemp}
	Return
}
3 up::Send, {1 Up}


+4::Send, {*}
4::
{
	if GetKeyState("CapsLock", "T")
		Send, {* DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {* DownTemp}
	else
		Send, {0 DownTemp}
	Return
}
4 up::Send, {0 Up}


5::Send, 4
6::Send, 7


+7::Send, {^}
7::
{
	if GetKeyState("CapsLock", "T")
		Send, {^ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {^ DownTemp}
	else
		Send, {6 DownTemp}
	Return
}
7 up::Send, {6 Up}


+8::Send, {`%}
8::
{
	if GetKeyState("CapsLock", "T")
		Send, {`% DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {`% DownTemp}
	else
		Send, {5 DownTemp}
	Return
}
8 up::Send, {5 Up}


+9::Send, {~}
9::
{
	if GetKeyState("CapsLock", "T")
		Send, {~ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {~ DownTemp}
	else
		Send, {9 DownTemp}
	Return
}
9 up::Send, {9 Up}


0::Send, 8


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Top row

+'::Send, {Q DownTemp}
'::
{
	if GetKeyState("CapsLock", "T")
		Send, {tab DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {- DownTemp}
	else
		Send, {q DownTemp}
	Return
}
' up::Send, {q Up}


+,::Send, {H DownTemp}
,::
{
	if GetKeyState("CapsLock", "T")
		Send, {< DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {5 DownTemp}
	else
		Send, {h DownTemp}
	Return
}
, up::Send, {h Up}


+.::Send, {O DownTemp}
.::
{
	if GetKeyState("CapsLock", "T")
		Send, {$ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {2 DownTemp}
	else
		Send, {o DownTemp}
	Return
}
. up::Send, {o Up}


+p::Send, {U DownTemp}
p::
{
	if GetKeyState("CapsLock", "T")
		Send, {> DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {3 DownTemp}
	else
		Send, {u DownTemp}
	Return
}
p up::Send, {u Up}


+y::Send, {X DownTemp}
y::
{
	if GetKeyState("NumLock", "T")
		Send, {: DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {X DownTemp}
	else
		Send, {x DownTemp}
	Return
}
y up::Send, {x Up}


f::Send, g
+f::Send, {G DownTemp}
;f::
;{
;	if GetKeyState("CapsLock", "T")
;		Send, {& DownTemp}
;	else
;		Send, {g DownTemp}
;	Return
;}
;f up::Send, {g Up}


+g::Send, {C DownTemp}
g::
{
	if GetKeyState("CapsLock", "T")
		Send, {[ DownTemp}
	else
		Send, {c DownTemp}
	Return
}
g up::Send, {c Up}


+c::Send, {R DownTemp}
c::
{
	if GetKeyState("CapsLock", "T")
		Send, {_ DownTemp}
	else
		Send, {r DownTemp}
	Return
}
c up::Send, {r Up}


+r::Send, {F DownTemp}
r::
{
	if GetKeyState("CapsLock", "T")
		Send, {] DownTemp}
	else
		Send, {f DownTemp}
	Return
}
r up::Send, {f Up}


l::Send, z
+l::Send, {Z DownTemp}
;l::
;{
;	if GetKeyState("CapsLock", "T")
;		Send, {| DownTemp}
;	else
;		Send, {v DownTemp}
;	Return
;}
;l up::Send, {v Up}


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Home row

`::Send, {-}

+a::Send, {Y DownTemp}
a::
{
	if GetKeyState("CapsLock", "T")
		Send, {\ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {7 DownTemp}
	else
		Send, {y DownTemp}
	Return
}
a up::Send, {y Up}


+o::Send, {I DownTemp}
o::
{
	if GetKeyState("CapsLock", "T")
		Send, {( DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {. DownTemp}
	else
		Send, {i DownTemp}
	Return
}
o up::Send, {i Up}


+e::Send, {E DownTemp}
e::
{
	if GetKeyState("CapsLock", "T")
		Send, {" DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {1 DownTemp}
	else
		Send, {e DownTemp}
	Return
}
e up::Send, {e Up}


+u::Send, {A DownTemp}
u::
{
	if GetKeyState("CapsLock", "T")
		Send, {) DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {0 DownTemp}
	else
		Send, {a DownTemp}
	Return
}
u up::Send, {a Up}


+i::Send, {@ DownTemp}
i::
{
	if GetKeyState("CapsLock", "T")
		Send, {# DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {4 DownTemp}
	else
		Send, {. DownTemp}
	Return
}
i up::Send, {. Up}


;+d::Send, {D DownTemp}
;d::
;{
;	if GetKeyState("CapsLock", "T")
;		Send, {D DownTemp}
;	else
;		Send, {d DownTemp}
;	Return
;}
;d up::Send, {d Up}


+h::Send, {S DownTemp}
h::
{
	if GetKeyState("CapsLock", "T")
		Send, {`{ DownTemp}
	else
		Send, {s DownTemp}
	Return
}
h up::Send, {s Up}


+t::Send, {T DownTemp}
t::
{
	if GetKeyState("CapsLock", "T")
		Send, {= DownTemp}
	else
		Send, {t DownTemp}
	Return
}
t up::Send, {t Up}


+n::Send, {N DownTemp}
n::
{
	if GetKeyState("CapsLock", "T")
		Send, {`} DownTemp}
	else
		Send, {n DownTemp}
	Return
}
n up::Send, {n Up}


+s::Send, {B DownTemp}
s::
{
	if GetKeyState("CapsLock", "T")
		Send, {| DownTemp}
	else
		Send, {b DownTemp}
	Return
}
s up::Send, {b Up}


\::Send, {;}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bottom row

+SC02C::Send, {J DownTemp}
SC02C::
{
	if GetKeyState("NumLock", "T")
		Send, {/ DownTemp}
	else if GetKeyState("CapsLock", "T")
		Send, {J DownTemp}
	else
		Send, {j DownTemp}
	Return
}
SC02C up::Send, {j Up}


+q::Send, {? DownTemp}
q::
{
	if GetKeyState("CapsLock", "T")
		Send, {: DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {6 DownTemp}
	else
		Send, {/ DownTemp}
	Return
}
q up::Send, {/ Up}


+j::Send, {! DownTemp}
j::
{
	if GetKeyState("CapsLock", "T")
		Send, {* DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {9 DownTemp}
	else
		Send, {, DownTemp}
	Return
}
j up::Send, {, Up}


+k::Send, {K DownTemp}
k::
{
	if GetKeyState("CapsLock", "T")
		Send, {+ DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {8 DownTemp}
	else
		Send, {k DownTemp}
	Return
}
k up::Send, {k Up}


+x::Send, {`` DownTemp}
x::
{
	if GetKeyState("CapsLock", "T")
		Send, {`` DownTemp}
	else if GetKeyState("NumLock", "T")
		Send, {, DownTemp}
	else
		Send, {' DownTemp}
	Return
}
x up::Send, {' Up}


b::w
;+b::Send, {W DownTemp}
;b::
;{
;	if GetKeyState("CapsLock", "T")
;		Send, {W DownTemp}
;	else
;		Send, {w DownTemp}
;	Return
;}
;b up::Send, {w Up}


+m::Send, {M DownTemp}
m::
{
	if GetKeyState("CapsLock", "T")
		Send, {& DownTemp}
	else
		Send, {m DownTemp}
	Return
}
m up::Send, {m Up}


+w::Send, {L DownTemp}
w::
{
	if GetKeyState("CapsLock", "T")
		Send, {^ DownTemp}
	else
		Send, {l DownTemp}
	Return
}
w up::Send, {l Up}


+v::Send, {P DownTemp}
v::
{
	if GetKeyState("CapsLock", "T")
		Send, {~ DownTemp}
	else
		Send, {p DownTemp}
	Return
}
v up::Send, {p Up}


z::v
;+z::Send, {V DownTemp}
;z::
;{
;	if GetKeyState("CapsLock", "T")
;		Send, {V DownTemp}
;	else
;		Send, {v DownTemp}
;	Return
;}
;z up::Send, {v Up}



#UseHook off

;
