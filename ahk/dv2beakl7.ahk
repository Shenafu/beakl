; Balanced Effortless Advanced Keyboard Layout
; convert dvorak to the BEAKL7 layout

;UNSHIFTED
;vrldb .uoiz
;wsthf ,naey
;kcmgx jpq"'

;SHIFTED
;VRLDB (UOIZ
;WSTHF )NAEY
;KCMGX JPQ;:

#SingleInstance force
#InstallKeybdHook
#UseHook on

;; Top row
'::Send, v
,::Send, r
.::Send, l
p::Send, d
y::Send, b
f::Send, .
g::Send, u
c::Send, o
r::Send, i
l::Send, z

+'::Send, V
+,::Send, R
+.::Send, L
+p::Send, D
+y::Send, B
+f::Send, {(}
+g::Send, U
+c::Send, O
+r::Send, I
+l::Send, Z

;; Home row
a::Send, w
o::Send, s
e::Send, t
u::Send, h
i::Send, f
d::Send, ,
h::Send, n
t::Send, a
n::Send, e
s::Send, y

+a::Send, W
+o::Send, S
+e::Send, T
+u::Send, H
+i::Send, F
+d::Send, {)}
+h::Send, N
+t::Send, A
+n::Send, E
+s::Send, Y

;; Bottom row
SC02C::Send, k
q::Send, c
j::Send, m
k::Send, g
x::Send, x
b::Send, j
m::Send, p
w::Send, q
v::Send, "
z::Send, '

+SC02C::Send, K
+q::Send, C
+j::Send, M
+k::Send, G
+x::Send, X
+b::Send, J
+m::Send, P
+w::Send, Q
+v::Send, {;}
+z::Send, :

;; Number row
;`::Send, 7
;1::Send, 9
;2::Send, 5
;3::Send, 1
;4::Send, 3
;5::Send, =
;6::Send, /
;7::Send, 2
;8::Send, 0
;9::Send, 4
;0::Send, 6
;[::Send, 8
;]::Send, \

;+`::Send, {SC029} ; backquote
;+1::Send, +2
;+2::Send, +[
;+3::Send, +/
;+4::Send, +5
;+5::Send, {+}
;+6::Send, +8
;+7::Send, +7
;+8::Send, +1
+9::Send, <
+0::Send, >
;+[::Send, +6
;+]::Send, +4

#UseHook off

;
