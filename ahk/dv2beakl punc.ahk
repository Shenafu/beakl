; https://ieants.cc/smf/index.php?topic=186.msg2719#msg2719

;Punc
;  @$#%    ~^`
;  <=>  {[_]}
; \(-)+ %{;}!
;  *:/   -> |~& !=

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Variables

; Upper Row
; SC009 = '8'
LAYOUT_KEYS["SC002"]["punc"] := "+{tab}"
LAYOUT_KEYS["+SC002"]["punc"] := "+{tab}"
LAYOUT_KEYS["SC003"]["punc"] := "@"
LAYOUT_KEYS["+SC003"]["punc"] := "@"
LAYOUT_KEYS["SC004"]["punc"] := "$"
LAYOUT_KEYS["+SC004"]["punc"] := "$"
LAYOUT_KEYS["SC005"]["punc"] := "{#}"
LAYOUT_KEYS["+SC005"]["punc"] := "{#}"
LAYOUT_KEYS["SC006"]["punc"] := "{tab}"
LAYOUT_KEYS["+SC006"]["punc"] := "{tab}"
LAYOUT_KEYS["SC007"]["punc"] := "7"
LAYOUT_KEYS["+SC007"]["punc"] := "7"
LAYOUT_KEYS["SC008"]["punc"] := "~"
LAYOUT_KEYS["+SC008"]["punc"] := "~"
LAYOUT_KEYS["SC009"]["punc"] := "{^}"
LAYOUT_KEYS["+SC009"]["punc"] := "{^}"
LAYOUT_KEYS["SC00A"]["punc"] := "``"
LAYOUT_KEYS["+SC00A"]["punc"] := "``"
LAYOUT_KEYS["SC00B"]["punc"] := "8"
LAYOUT_KEYS["+SC00B"]["punc"] := "8"

; Top Row
LAYOUT_KEYS["'"]["punc"] := "+{tab}"
LAYOUT_KEYS["+'"]["punc"] := "+{tab}"
LAYOUT_KEYS[","]["punc"] := "<"
LAYOUT_KEYS["+,"]["punc"] := "<"
LAYOUT_KEYS["."]["punc"] := "="
LAYOUT_KEYS["+."]["punc"] := "="
LAYOUT_KEYS["p"]["punc"] := ">"
LAYOUT_KEYS["+p"]["punc"] := ">"
LAYOUT_KEYS["y"]["punc"] := "{tab}"
LAYOUT_KEYS["+y"]["punc"] := "{tab}"
LAYOUT_KEYS["f"]["punc"] := "{{}"
LAYOUT_KEYS["+f"]["punc"] := "{{}"
LAYOUT_KEYS["g"]["punc"] := "["
LAYOUT_KEYS["+g"]["punc"] := "["
LAYOUT_KEYS["c"]["punc"] := "_"
LAYOUT_KEYS["+C"]["punc"] := "_"
LAYOUT_KEYS["r"]["punc"] := "]"
LAYOUT_KEYS["+r"]["punc"] := "]"
LAYOUT_KEYS["l"]["punc"] := "{}}"
LAYOUT_KEYS["+L"]["punc"] := "{}}"

; Home Row
LAYOUT_KEYS["a"]["punc"] := "\"
LAYOUT_KEYS["+a"]["punc"] := "\"
LAYOUT_KEYS["o"]["punc"] := "("
LAYOUT_KEYS["+o"]["punc"] := "("
LAYOUT_KEYS["e"]["punc"] := "-"
LAYOUT_KEYS["+e"]["punc"] := "-"
LAYOUT_KEYS["u"]["punc"] := ")"
LAYOUT_KEYS["+u"]["punc"] := ")"
LAYOUT_KEYS["i"]["punc"] := "{NumpadAdd}"
LAYOUT_KEYS["+i"]["punc"] := "{+}"
LAYOUT_KEYS["d"]["punc"] := "%"
LAYOUT_KEYS["+d"]["punc"] := "%"
LAYOUT_KEYS["h"]["punc"] := "{{}"
LAYOUT_KEYS["+h"]["punc"] := "{{}"
LAYOUT_KEYS["t"]["punc"] := ";"
LAYOUT_KEYS["+t"]["punc"] := ";"
LAYOUT_KEYS["n"]["punc"] := "{}}"
LAYOUT_KEYS["+n"]["punc"] := "{}}"
LAYOUT_KEYS["s"]["punc"] := "{!}"
LAYOUT_KEYS["+s"]["punc"] := "{!}"
LAYOUT_KEYS["\"]["punc"] := ";"
LAYOUT_KEYS["+\"]["punc"] := ";"

; Bottom Row
LAYOUT_KEYS[";"]["punc"] := "{tab}"
LAYOUT_KEYS["+;"]["punc"] := "+{tab}"
LAYOUT_KEYS["q"]["punc"] := "*"
LAYOUT_KEYS["+q"]["punc"] := "*"
LAYOUT_KEYS["j"]["punc"] := ":"
LAYOUT_KEYS["+j"]["punc"] := ":"
LAYOUT_KEYS["k"]["punc"] := "/"
LAYOUT_KEYS["+k"]["punc"] := "/"
LAYOUT_KEYS["x"]["punc"] := "{enter}"
LAYOUT_KEYS["+x"]["punc"] := "+{enter}"
LAYOUT_KEYS["b"]["punc"] := "->"
LAYOUT_KEYS["+b"]["punc"] := "->"
LAYOUT_KEYS["m"]["punc"] := "|"
LAYOUT_KEYS["+m"]["punc"] := "|"
LAYOUT_KEYS["w"]["punc"] := "~"
LAYOUT_KEYS["+w"]["punc"] := "~"
LAYOUT_KEYS["v"]["punc"] := "&"
LAYOUT_KEYS["+v"]["punc"] := "&"
LAYOUT_KEYS["z"]["punc"] := "{!}="
LAYOUT_KEYS["+z"]["punc"] := "{!}="
