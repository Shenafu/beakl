; Balanced Effortless Advanced Keyboard Layout
; convert dvorak to the BEAKL Opted layout

;UNSHIFTED
;  40123 87659
;j':",) fylpvq
; khea. cstrb
;x-uio( gdmnwz

;SHIFTED
;  `#$!& |@*%^
;J\;</> FYLPVQ
; KHEA_ CSTRB
;X?UIO~ GDMNWZ

#SingleInstance force
;#InstallKeybdHook
#UseHook on

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; TOP ROW
`::Send, j

'::Send, '

,::
{
if not GetKeyState("Numlock", "T")
	Send, {: DownTemp}
else
	Send, {2 DownTemp}
Return
}
, up::Send, {: Up}

.::
{
if not GetKeyState("Numlock", "T")
	Send, {" DownTemp}
else
	Send, {9 DownTemp}
Return
}
. up::Send, {" Up}

p::
{
if not GetKeyState("Numlock", "T")
	Send, {, DownTemp}
else
	Send, {7 DownTemp}
Return
}
p up::Send, {, Up}

y::
{
if not GetKeyState("Numlock", "T")
	Send, {) DownTemp}
else
	Send, {6 DownTemp}
Return
}
y up::Send, {) Up}

f::Send, f
g::Send, y
c::Send, l
r::Send, p
l::Send, v

\::Send, q

~::Send, J

+'::Send, \
+,::Send, {;}
+.::Send, <
+p::Send, /
+y::Send, >
+f::Send, F
+g::Send, Y
+c::Send, L
+r::Send, P
+l::Send, V

|::Send, Q

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; HOME ROW
a::
{
if not GetKeyState("Numlock", "T")
	Send, {k DownTemp}
else
	Send, {5 DownTemp}
Return
}
a up::Send, {k Up}

o::
{
if not GetKeyState("Numlock", "T")
	Send, {h DownTemp}
else
	Send, {1 DownTemp}
Return
}
o up::Send, {h Up}

e::
{
if not GetKeyState("Numlock", "T")
	Send, {e DownTemp}
else
	Send, {0 DownTemp}
Return
}
e up::Send, {e Up}

u::
{
if not GetKeyState("Numlock", "T")
	Send, {a DownTemp}
else
	Send, {3 DownTemp}
Return
}
u up::Send, {a Up}

i::Send, {. DownTemp}
i up::Send, {. Up}

d::Send, c
h::Send, s
t::Send, t
n::Send, r
s::Send, b

+a::Send, K
+o::Send, H
+e::Send, E
+u::Send, A
+i::Send, _
+d::Send, C
+h::Send, S
+t::Send, T
+n::Send, R
+s::Send, B

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; BOTTOM ROW
/::Send, x

SC02C::Send, -

q::
if not GetKeyState("Numlock", "T")
	Send, {u DownTemp}
else
	Send, {4 DownTemp}
Return
q up::Send, {u up}

j::
{
if not GetKeyState("Numlock", "T")
	Send, {i DownTemp}
else
	Send, {8 DownTemp}
Return
}
j up::Send, {i up}

k::
{
if not GetKeyState("Numlock", "T")
	Send, {o DownTemp}
else
	Send, {/ DownTemp}
Return
}
k up::Send, {o up}

x::Send, (
b::Send, g
m::Send, d
w::Send, m
v::Send, n
z::Send, w

-::Send, z

+/::Send, X

+SC02C::Send, ?
+q::Send, U
+j::Send, I
+k::Send, O
+x::Send, ~
+b::Send, G
+m::Send, D
+w::Send, M
+v::Send, N
+z::Send, W

+-::Send, Z

;; Number row
1::Send, 4
2::Send, 0
3::Send, 1
4::Send, 2
5::Send, 3
6::Send, 8
7::Send, 7
8::Send, 6
9::Send, 5
0::Send, 9

+1::Send, ``
+2::Send, {#}
+3::Send, $
+4::Send, {!}
+5::Send, &
+6::Send, |
+7::Send, @
+8::Send, *
+9::Send, `%
+0::Send, {^}

#UseHook off

;
