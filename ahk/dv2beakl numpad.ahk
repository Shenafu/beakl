; https://ieants.cc/smf/index.php?topic=186.msg2719#msg2719


; Upper Row
; l0
;  32104 76598

;S0
;  !@$#% ^(*)&

;Num
;  +/*=   yxz
; -523:  ~FED
; 7.104  {CBA}
; ,689    []%

;SNum
;  @$#%   YXZ
; !![]=      _fed
; &$#%|  {cba}
; ;^<>     ()\

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Variables

; Upper Row
; SC009 = '8'
LAYOUT_KEYS["SC002"]["l0"] := "3"
LAYOUT_KEYS["+SC002"]["l0"] := "{!}"
LAYOUT_KEYS["SC003"]["l0"] := "2"
LAYOUT_KEYS["+SC003"]["l0"] := "@"
LAYOUT_KEYS["SC004"]["l0"] := "1"
LAYOUT_KEYS["+SC004"]["l0"] := "$"
LAYOUT_KEYS["SC005"]["l0"] := "{0}"
LAYOUT_KEYS["+SC005"]["l0"] := "{#}"
LAYOUT_KEYS["SC006"]["l0"] := "4"
LAYOUT_KEYS["+SC006"]["l0"] := "%"
LAYOUT_KEYS["SC007"]["l0"] := "7"
LAYOUT_KEYS["+SC007"]["l0"] := "{^}"
LAYOUT_KEYS["SC008"]["l0"] := "6"
LAYOUT_KEYS["+SC008"]["l0"] := "("
LAYOUT_KEYS["SC009"]["l0"] := "5"
LAYOUT_KEYS["+SC009"]["l0"] := "*"
LAYOUT_KEYS["SC00A"]["l0"] := "9"
LAYOUT_KEYS["+SC00A"]["l0"] := ")"
LAYOUT_KEYS["SC00B"]["l0"] := "8"
LAYOUT_KEYS["+SC00B"]["l0"] := "&"

LAYOUT_KEYS["SC002"]["num"] := "{tab}"
LAYOUT_KEYS["+SC002"]["num"] := "+{tab}"
LAYOUT_KEYS["SC003"]["num"] := "{NumpadAdd}"
LAYOUT_KEYS["+SC003"]["num"] := "@"
LAYOUT_KEYS["SC004"]["num"] := "/"
LAYOUT_KEYS["+SC004"]["num"] := "$"
LAYOUT_KEYS["SC005"]["num"] := "*"
LAYOUT_KEYS["+SC005"]["num"] := "{#}"
LAYOUT_KEYS["SC006"]["num"] := "="
LAYOUT_KEYS["+SC006"]["num"] := "%"
LAYOUT_KEYS["SC007"]["num"] := "7"
LAYOUT_KEYS["+SC007"]["num"] := "7"
LAYOUT_KEYS["SC008"]["num"] := "y"
LAYOUT_KEYS["+SC008"]["num"] := "Y"
LAYOUT_KEYS["SC009"]["num"] := "x"
LAYOUT_KEYS["+SC009"]["num"] := "X"
LAYOUT_KEYS["SC00A"]["num"] := "z"
LAYOUT_KEYS["+SC00A"]["num"] := "Z"
LAYOUT_KEYS["SC00B"]["num"] := "8"
LAYOUT_KEYS["+SC00B"]["num"] := "8"

; Top Row
LAYOUT_KEYS["'"]["num"] := "-"
LAYOUT_KEYS["+'"]["num"] := "{!}="
LAYOUT_KEYS[","]["num"] := "5"
LAYOUT_KEYS["+,"]["num"] := "{!}"
LAYOUT_KEYS["."]["num"] := "2"
LAYOUT_KEYS["+."]["num"] := "["
LAYOUT_KEYS["p"]["num"] := "3"
LAYOUT_KEYS["+p"]["num"] := "]"
LAYOUT_KEYS["y"]["num"] := ":"
LAYOUT_KEYS["+y"]["num"] := "="
LAYOUT_KEYS["f"]["num"] := "~"
LAYOUT_KEYS["+f"]["num"] := "_"
LAYOUT_KEYS["g"]["num"] := "F"
LAYOUT_KEYS["+g"]["num"] := "f"
LAYOUT_KEYS["c"]["num"] := "E"
LAYOUT_KEYS["+C"]["num"] := "e"
LAYOUT_KEYS["r"]["num"] := "D"
LAYOUT_KEYS["+r"]["num"] := "d"
LAYOUT_KEYS["l"]["num"] := "{}}"
LAYOUT_KEYS["+L"]["num"] := "{}}"

; Home Row
LAYOUT_KEYS["a"]["num"] := "7"
LAYOUT_KEYS["+a"]["num"] := "&"
LAYOUT_KEYS["o"]["num"] := "."
LAYOUT_KEYS["+o"]["num"] := "$"
LAYOUT_KEYS["e"]["num"] := "1"
LAYOUT_KEYS["+e"]["num"] := "{#}"
LAYOUT_KEYS["u"]["num"] := "{0}"
LAYOUT_KEYS["+u"]["num"] := "%"
LAYOUT_KEYS["i"]["num"] := "4"
LAYOUT_KEYS["+i"]["num"] := "|"
LAYOUT_KEYS["d"]["num"] := "{{}"
LAYOUT_KEYS["+d"]["num"] := "{{}"
LAYOUT_KEYS["h"]["num"] := "C"
LAYOUT_KEYS["+h"]["num"] := "c"
LAYOUT_KEYS["t"]["num"] := "B"
LAYOUT_KEYS["+t"]["num"] := "b"
LAYOUT_KEYS["n"]["num"] := "A"
LAYOUT_KEYS["+n"]["num"] := "a"
LAYOUT_KEYS["s"]["num"] := "{}}"
LAYOUT_KEYS["+s"]["num"] := "{}}"
LAYOUT_KEYS["\"]["num"] := ";"
LAYOUT_KEYS["+\"]["num"] := ";"

; Bottom Row
LAYOUT_KEYS[";"]["num"] := ","
LAYOUT_KEYS["+;"]["num"] := ";"
LAYOUT_KEYS["q"]["num"] := "6"
LAYOUT_KEYS["+q"]["num"] := "<"
LAYOUT_KEYS["j"]["num"] := "9"
LAYOUT_KEYS["+j"]["num"] := "{^}"
LAYOUT_KEYS["k"]["num"] := "8"
LAYOUT_KEYS["+k"]["num"] := ">"
LAYOUT_KEYS["x"]["num"] := "{enter}"
LAYOUT_KEYS["+x"]["num"] := "+{enter}"
LAYOUT_KEYS["b"]["num"] := "{^}"
LAYOUT_KEYS["+b"]["num"] := "{^}"
LAYOUT_KEYS["m"]["num"] := "("
LAYOUT_KEYS["+m"]["num"] := "("
LAYOUT_KEYS["w"]["num"] := ")"
LAYOUT_KEYS["+w"]["num"] := ")"
LAYOUT_KEYS["v"]["num"] := "\"
LAYOUT_KEYS["+v"]["num"] := "\"
LAYOUT_KEYS["z"]["num"] := "%"
LAYOUT_KEYS["+z"]["num"] := "%"
