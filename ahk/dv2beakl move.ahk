; https://ieants.cc/smf/index.php?topic=186.msg2719#msg2719

;Move
;								WheelUp
;	Tab			PgUp		Up     			Enter	Copy
;	Home		Left		Down    		Right	End
;	R-Click		PgDn		WheelDown	Click		Paste

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Variables

; Upper Row
; SC009 = '8'
LAYOUT_KEYS["SC004"]["move"] := "{WheelUp}"
LAYOUT_KEYS["+SC004"]["move"] := "+{WheelUp}"

; Top Row
LAYOUT_KEYS["'"]["move"] := "{Tab}"
LAYOUT_KEYS["+'"]["move"] := "+{Tab}"
LAYOUT_KEYS[","]["move"] := "{PgUp}"
LAYOUT_KEYS["+,"]["move"] := "+{PgUp}"
LAYOUT_KEYS["."]["move"] := "{Up}"
LAYOUT_KEYS["+."]["move"] := "+{Up}"
LAYOUT_KEYS["p"]["move"] := "{Enter}"
LAYOUT_KEYS["+p"]["move"] := "+{Enter}"
LAYOUT_KEYS["y"]["move"] := "^c"
LAYOUT_KEYS["+y"]["move"] := "+^c"

; Home Row
LAYOUT_KEYS["a"]["move"] := "{Home}"
LAYOUT_KEYS["+a"]["move"] := "+{Home}"
LAYOUT_KEYS["o"]["move"] := "{Left}"
LAYOUT_KEYS["+o"]["move"] := "+{Left}"
LAYOUT_KEYS["e"]["move"] := "{Down}"
LAYOUT_KEYS["+e"]["move"] := "+{Down}"
LAYOUT_KEYS["u"]["move"] := "{Right}"
LAYOUT_KEYS["+u"]["move"] := "+{Right}"
LAYOUT_KEYS["i"]["move"] := "{End}"
LAYOUT_KEYS["+i"]["move"] := "+{End}"

; Bottom Row
LAYOUT_KEYS[";"]["move"] := "{RButton}"
LAYOUT_KEYS["+;"]["move"] := "+{RButton}"
LAYOUT_KEYS["q"]["move"] := "{PgDn}"
LAYOUT_KEYS["+q"]["move"] := "+{PgDn}"
LAYOUT_KEYS["j"]["move"] := "{WheelDown}"
LAYOUT_KEYS["+j"]["move"] := "+{WheelDown}"
LAYOUT_KEYS["k"]["move"] := "{LButton}"
LAYOUT_KEYS["+k"]["move"] := "+{LButton}"
LAYOUT_KEYS["x"]["move"] := "^v"
LAYOUT_KEYS["+x"]["move"] := "+^v"
