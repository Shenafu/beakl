; Balanced Effortless Advanced Keyboard Layout
; convert dvorak to the BEAKL layout

;UNSHIFTED
;~ 73159 84026 =
;- ,gdyk bwhu. _
;: roenl mtias ;
;\ "qjcx pfvz' /

;SHIFTED
;` !@#$% ^&*+
;] [GDYK BWHU< >
;| ROENL MTIAS ?
;) (QJCX PFVZ{ }

#SingleInstance force
#InstallKeybdHook
#UseHook on

;; Top row
'::Send, {,}
,::Send, g
.::Send, d
p::Send, y
y::Send, k
f::Send, b
g::Send, w
c::Send, h
r::Send, u
l::Send, .

+'::Send, (
+,::Send, G
+.::Send, D
+p::Send, Y
+y::Send, K
+f::Send, B
+g::Send, W
+c::Send, H
+r::Send, U
+l::Send, )

;; Home row
a::Send, r
o::Send, o
e::Send, e
u::Send, n
i::Send, l
d::Send, m
h::Send, t
t::Send, i
n::Send, a
s::Send, s

+a::Send, R
+o::Send, O
+e::Send, E
+u::Send, N
+i::Send, L
+d::Send, M
+h::Send, T
+t::Send, I
+n::Send, A
+s::Send, S

;; Bottom row
SC02C::Send, "
q::Send, q
j::Send, j
k::Send, c
x::Send, x
b::Send, p
m::Send, f
w::Send, v
v::Send, z
z::Send, '

+SC02C::Send, :
+q::Send, Q
+j::Send, J
+k::Send, C
+x::Send, X
+b::Send, P
+m::Send, F
+w::Send, V
+v::Send, Z
+z::Send, {;}

;; Number row
;`::Send, ~
;1::Send, 7
;2::Send, 3
;3::Send, 1
;4::Send, 5
;5::Send, 9
;6::Send, 8
;7::Send, 4
;8::Send, 0
;9::Send, 2
;0::Send, 6
;[::Send, [
;]::Send, _

;+`::Send, {SC029} ; backquote
;+1::Send, +2
;+2::Send, +[
;+3::Send, +/
;+4::Send, +5
;+5::Send, {+}
;+6::Send, +8
;+7::Send, +7
;+8::Send, +1
+9::Send, <
+0::Send, >
;+[::Send, +6
;+]::Send, +4

#UseHook off

;
