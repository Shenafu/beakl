<!doctype html>
<html>
<head>
<title>BEAKL Practice Typing Tutor</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="beakl.css" rel="stylesheet" type="text/css">
<script src="/lib/markdown.js"></script>
<script src="beakl.js"></script>
</head>

<body onload="Page_Onload();">

<?php include('navbar.php'); ?>

<div class='content-main'>
<pre>
# BEAKL Practice Typing Tutor

## Home Keys

aaa eee iii aei aei aei iea iea iea ae ae ae ei ei ei ai ai ai ea ea ea ie ie ie ia ia ia
ttt rrr sss trs trs trs srt srt srt tr tr tr rs rs rs ts ts ts rt rt rt sr sr sr st st st
at at at ar ar ar as as as et et et er er er es es es it it it ir ir ir is is is
ta ta ta te te te ti ti ti ra ra ra re re re ri ri ri sa sa sa se se se si si si
Sara eats at seat tastes rat tears
Terrier retreat taser setter irate
Tsar tries rites stars airs sea site
Retire attire resit tree tire rest area
Stir tea sir sear arise sets retries
</pre>

<form><textarea name="1" rows=10 cols=80></textarea><input type=RESET value=Clear></form>

<pre>
## Top Keys

... ooo uuu .ou .ou .ou uo. uo. uo. .o .o .o ou ou ou .u .u .u o. o. o. uo uo uo u. u. u.
mmm nnn ddd mnd mnd mnd dnm dnm dnm mn mn mn nd nd nd md md md nm nm nm dn dn dn dm dm dm
.m .m .m .n .n .n .d .d .d om om om on on on od od od um um um un un un ud ud ud
m. m. m. mo mo mo mu mu mu n. n. n. no no no nu nu nu d. d. d. do do do du du du
Noon udon umm nom nod.
Mom dunno duo dumdum mood.
Moon mondo doudou noo mound.
Doom momo nun don duomo.
</pre>

<form><textarea name="2" rows=10 cols=80></textarea><input type=RESET value=Clear></form>

<pre>
## Home + Top Keys

a. ao au am an ad e. eo eu em en ed i. io iu im in id s. so su sm sn sd r. ro ru rm rn rd t. to tu tm tn td .a .e .i .t .r .s oa oe oi ot or os da de di dt dr ds na ne ni nt nr ns ma me mi mt mr ms
Missed mantou dinner so ran under distress.
Modern art soon dismisses man made items.
Nutritionists endorse to eat iron diet more.
Demure maidens ride into sunset amidst ruins.
Sometimes seeds in dirt raises into trees.
Do not ruminate. Minutes mean more to dinosaur eons.
Unmonitored mountaineers domination unrestrained.
</pre>

<form><textarea name="3" rows=10 cols=80></textarea><input type=RESET value=Clear></form>

<pre>
## Bottom + Outside Keys

hhh ''' /// yyy kkk ppp fff lll ccc ggg
hphp 'f'f /l/l ycyc kgkg phph f'f' l/l/ cycy gkgk
ph ph gh gh ck ck lk lk cl cl fl fl ch ch fy fy 'll 'll f/g/h phy phy cky cky lly lly fly fly ply ply y'l y'l
the she pear https light/dark ought home called match asphalt once sleet please flounder fast try apply he/she it's don't hasn't he'll thank you stalker from if actors .com/ .net/ play they yes okay ally March stitches dancing strong lungs findings stayed rough nymph actually lymphoid there sheath either   strengthen construction you're aren't isn't

</pre>

<form><textarea name="4" rows=10 cols=80></textarea><input type=RESET value=Clear></form>

<pre>
## Corner Keys
</pre>

<form><textarea name="5" rows=10 cols=80></textarea><input type=RESET value=Clear></form>

</div>

</body>
</html>
