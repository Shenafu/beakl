<!doctype html>
<html>
<head>
<title>BEAKL Downloads</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="beakl.css" rel="stylesheet" type="text/css">
<script src="/lib/markdown.js"></script>
<script src="beakl.js"></script>
</head>

<body onload="Page_Onload();">

<?php include('navbar.php'); ?>

<div class='content-main'>
	<pre>
# BEAKL Downloads

## XKB

### BEAKL

- [Rules](https://bitbucket.org/Shenafu/beakl/src/master/xkb/rules/beakl.xml) : copy contents into /usr/share/X11/xkb/rules/evdev.extras.xml or evdev.xml, within layoutList section
- [Symbols](https://bitbucket.org/Shenafu/beakl/src/master/xkb/symbols/beakl) : copy file into directory /usr/share/X11/xkb/symbols/
- [default keyboard](https://bitbucket.org/Shenafu/beakl/src/master/xkb/symbols/beakl) : copy file into directory /etc/default/
- Defaults to BEAKL 27 layout

## Opt

### Configuration

Compile and run ***opt*** that accepts 31-keys, including space.

- [beakl30.cfg](https://bitbucket.org/Shenafu/beakl/src/master/opt/beakl30.cfg)


### Corpora

- [xaycorpus.txt.1](https://bitbucket.org/Shenafu/beakl/src/master/opt/xaycorpus.txt.1)
- [xaycorpus.txt.2](https://bitbucket.org/Shenafu/beakl/src/master/opt/xaycorpus.txt.2)
- [xaycorpus.txt.3](https://bitbucket.org/Shenafu/beakl/src/master/opt/xaycorpus.txt.3)

-
	</pre>
</div>

</body>
</html>
