function createToc() {
	var toc = document.createElement('div');
	toc.className = "content-toc";
	toc.innerHTML = '<h2>Quick Nav</h2>';
	var ul = document.createElement('ul');
	toc.appendChild(ul);

	var headerClass = 'header';
	var mains = document.getElementsByClassName('content-main');
	var tags = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'];

	// set class for matching tags 
	Array.prototype.map.call(mains, function(main) {
		tags.map(function(tag) {
			var els = main.getElementsByTagName(tag);
			Array.prototype.map.call(els, function(el) {
				el.className = headerClass;
			});
		});
	});
	
	var headers = document.getElementsByClassName(headerClass);
	// iterate tags with class at above step
	Array.prototype.map.call(headers, function(header) {
		var title = header.innerText;
		var id = title.toLowerCase().replace(/ /g, '-');
		
		var li = document.createElement('li');
		ul.appendChild(li);
		
		var a = document.createElement('a');
		li.appendChild(a);
		a.innerText = title;
		a.href = "#" + id;
		
		header.id = id;
	});
	
	//document.body.insertBefore(toc, document.body.firstChild);
	document.body.appendChild(toc);
}

function renderMarkdown(element) {
	var text = element.innerHTML;
	element.innerHTML = markdown.toHTML(text);
};


function Page_Onload() {
	var mds = document.getElementsByTagName('pre');
	for (var i=0, max=mds.length; i<max; i++) {
		renderMarkdown( mds[i] );
	}
	
	createToc();
};
