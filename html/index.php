<!doctype html>
<html>
<head>
<title>BEAKL (Balanced Effortless Advanced Keyboard Layout)</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="beakl.css" rel="stylesheet" type="text/css">
<script src="/lib/markdown.js"></script>
<script src="beakl.js"></script>
</head>

<body onload="Page_Onload();">

<?php include('navbar.php'); ?>

<div class='content-main'>
	<pre>
# BEAKL

![BEAKL Logo](/images/kb-beakl-logo-2.png)

## Better Keyboard Layouts

BEAKL (Balanced Effortless Advanced Keyboard Layout) is a radical evolving theory and observations that strives to find the optimal efficient, ergonomic layout. The result is a series of progressively improved layouts as new discoveries and models are updated into the theory.

## Latest Recommended BEAKL Layout:

**BEAKL 27**: Outstanding performance for both English and non-English prose and code. In particular, excellent rolls, comfort, and intuitiveness. Easy to learn, quick to achieve fast typing speed.

![BEAKL 27](/images/kb-beakl-27-full.png)
	</pre>
	<code>
BEAKL 27 (main layer):

  32104 76598
  qhoux gcmrz
  yiea. dstnb
  j",k' wflpv

BEAKL 27 (shift layer):

  !@$#% ^(*)&
  QHOUX GCMRZ
  YIEA` DSTNB
  J!?K' WFLPV

BEAKL (punctuation layer):

  ↹@$#↹  ~^`
  ↹<=>   [_]
  \(-)+ %{;}!
   *:/⏎  |~&

BEAKL (numpad layer):

  ↹+/*=  yxz
  -523: ~FED
  7.104 {CBA}
  ,698⏎  []%

BEAKL (numpad layer shifted):

  ↹@$#%  YXZ
   ![]= _fed
  &$#%| {cba}
  ;^<>⏎  ()\

	</code>
	<pre>
## Customization

If you have issue with high same-finger hit rates on BEAKL 27, you may move some keys around. See below BEAKL 27-A. However, these changes rely more on pinky fingers, thus slower rolls involving the pinky. Nonetheless, it's a viable alteration that may suit your needs.

Any or all of the following may be swapped:
	N<>R
	P<>B
	H<>Y

The layout below, BEAKL 27-A, shows the final layout if all three swaps are made.
	</pre>
	<code>
BEAKL 27-A (main layer):

  qyoux gcmnz
  hiea. dstrp
  j",k' wflbv

BEAKL 27-A (shift layer):

  QYOUX GCMNZ
  HIEA` DSTRP
  J!?K' WFLBV
	</code>
	<pre>
## Learn More

* Learn about the [history of BEAKL](history.php)
	</pre>
</div>

</body>
</html>
