<!doctype html>
<html>
<head>
<title>BEAKL Principles</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="beakl.css" rel="stylesheet" type="text/css">
<script src="/lib/markdown.min.js"></script>
<script src="beakl.js"></script>
</head>

<body onload="Page_Onload();">

<?php include('navbar.php'); ?>

<div class='content-main'>
	<pre class="markdown">
# BEAKL Principles

BEAKL stands for Balanced Effortless Advanced Keyboard Layout. Its goals are as follows:

## Keyboard Layout Tenets

* Balance the workload between each hand and between the fingers and keys based on the fingers' strength and agility and the keys' potential for rolls and speed.
* Effortless means good rhythm in alternations and rolls, and getting a superior score in every keyboard metric, such as low distance, low finger usage, low penalties from same finger and same hands, etc. Possibly higher speed ceiling with less effort.
* Advanced in the sense that it challenges traditional theories of typing.
* It does not advocate the so-called home row in touch typing. It vehemently avoids favoring the home pinky and the inside index key. The home pinky is extremely slow, weak, and uncomfortable to type.
* Instead, it strongly recommends the home block that consists of the ring, index, and middle keys on the top, home, and bottom rows. These 9 keys form the core where the common letters (and sometimes punctuations) should be placed.
* Related to the above, the pinky and inside index columns workload must be minimized. Each of these columns should have no more than 5% of the total key presses. Together, the two pinkies should not do more than 5% of the work. The index home column already does a lot of work, so the inside column usage should be minimized.
* The bottom ring and index keys are extremely fast and should not be avoided as other keyboard designers suggest. These can be sometimes up to 2x or faster than pressing the home pinky key.
* Up and down finger movement is much faster, allows smoother rolls, and causes less strain than side-to-side movement (caused by pinky and inside index keys, and also by unnatural staggered keyboard design.)
* Exploit the powerful thumbs to their maximum potential. Can be used for common characters and modifier keys.
* Recommend additional layers that are accessed by thumb modifiers like shift, alt-gr, and numlock, so that fingers stay in the home block, rather than reaching all over the keyboard for numbers and symbols. This reduces time looking for symbols, speeds up writing code and math equations, provides an efficient numpad, and eliminates the need for tenkey numpad on the far right side of a full standard keyboard (thereby shrinking the size of the keyboard and allows mouse to be comfortably closer to the body).

## Effort Grid

An effort grid shows the penalty for each key on the keyboard. Keys with lower effort scores are considered preferable for common letters and punctuations. Hard to reach corner keys should be avoided.

Effort grids can assist in configuring optimization programs so that they can deliver the optimal layouts that heavily favor stronger fingers.

Examine a sample effort grid below:

![effort grid](/images/kb-keyboard_effort_grid_20180712.png "BEAKL effort grid")

## Continued Reading

* Peruse the [BEAKL layouts](layouts.php)
* Learn about the [history of BEAKL](history.php)
	</pre>
</div>

</body>
</html>
