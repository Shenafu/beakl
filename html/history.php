<!doctype html>
<html>
<head>
<title>BEAKL History</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="beakl.css" rel="stylesheet" type="text/css">
<script src="/lib/markdown.js"></script>
<script src="beakl.js"></script>
</head>

<body onload="Page_Onload();">

<?php include('navbar.php'); ?>

<div class='content-main'>
	<pre class="markdown">
# BEAKL History

[BEAKL](/code/keyboard/beakl/index.php) (Balanced Effortless Advanced Keyboard Layout) is a radical evolving theory and observations that strives to find the optimal efficient, ergonomic layout. The result is a series of progressively improved layouts as new discoveries and models are updated into the theory.

## Background

Many people have attempted to seek and design layouts that perform and feel better than the QWERTY found on the very first typewriter. Notable layouts include Dvorak, Maltron, Colemak, AdNW, MTGAP, Workman, Arensito, etc. and many many unknown layouts--some even patented, but none successful. Each have their own ideas and criteria on what is considered a good or perfect layout.

One such zealot fascinated with non standard layouts is Xay Voong. His journey began as a young man when he happened to find several internet articles that claimed that QWERTY is antiquated, inefficient, even harmful. They claimed that Dvorak is an improved, rational, tested layout, especially for touch typing. So during his military leave, he used this free time to teach himself to type using Dvorak. Within a week, he was able to get used to a radically different layout; within a couple of months, he achieved typing speeds faster than he ever did on QWERTY. He has continued to use Dvorak as his preferred layout at home and at work for many years, until he discovered BEAKL.

Amid his 15 years of utilization with Dvorak layout, Voong has periodically kept up with news and websites regarding keyboard layouts. During this time in early 21st century, there was quite a flurry of activity on nonstandard layouts and layout optimization. There were sites that compared and displayed the metrics of well-known layouts, such as QWERTY, Dvorak, Coleman, Workman, MTGAP, etc. Voong also endeavored to manually create his own layouts and compared them to others. The [Amuseum and Balanced](/layout.php) layouts were such early attempts; while these layouts performed equally well as others, however he was not satisfied with just good enough.

There are also online speed typing tutors and apps. All of them just show you how fast you type in words per minute. But there was only one particular site that shows you the speed and responsiveness by each finger. This finger by finger comparison was the breakthrough moment that enabled Voong to advance new superior keyboard layouts after years of stagnation.

Some programmers created software based on certain principles in order to wade past the billions of billions of possible permutations, which would be impossible for humans to manually compare. But with computer assistance, it takes mere seconds to find the optimal layout among millions of candidates, assuming that the criteria, configuration, and code are coherent and concordant. Voong was not able to exploit these earlier programs, as they required esoteric requirements beyond his means at that time; until some years later, he tried again with a more user-friendly layout optimization program called AdNW, a.k.a. Opt. With this program, he was finally able to put his theories and experience into a worthy contender that he is proud to promote to others who are also seeking improved alternative layouts.

Not long after this breakthrough, [Voong's personal forums](/smf/index.php?topic=89.0) welcomed a fellow by handle name of 'iandoug' (Ian Douglas), another keyboard enthusiast. He was adept at finding obscure sources of everything related to keyboard layouts and at keeping track of comparison scores between hundreds of layouts. Together the curious duo bounced ideas--old and new--back and forth continually, which in turn fed into and advanced the BEAKL theory at rapid progress.

## Development and Progress


## Continued Reading

* Continue reading about [BEAKL principles](beakl.php)
* Peruse the [BEAKL layouts](layouts.php)
	</pre>
</div>

</body>
</html>
