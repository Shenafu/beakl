<!doctype html>
<html>
<head>
<title>BEAKL Layouts</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="beakl.css" rel="stylesheet" type="text/css">
<script src="/lib/markdown.js"></script>
<script src="beakl.js"></script>
</head>

<body onload="Page_Onload();">

<?php include('navbar.php'); ?>

<div class='content-main'>
	<pre>
# BEAKL Layouts

The current recommended BEAKL Layout is <a href="#beakl-27-matrix">BEAKL 27</a>.

Continue to browse all the notable BEAKL layouts below. Don't forget the <a href="#punctuation">symbols</a> and <a href="#numpad">numpad</a> layers.

## BEAKL Opted

Layouts generated with the aid of Opt program. (In reverse chronological order.)

### beakl-30-matrix

![beakl-30-matrix](/images/kb-beakl-30-full.png)

It's almost a mirror of BEAKL Opted3; such that vowels are on the left and consonants on the right.


### beakl-27-matrix

![beakl-27-matrix](/images/kb-beakl-27-full.png)

BEAKL 27 is the current recommended layout. It improves on BEAKL 15 by reducing same fingers rate in the consonants district. The quotes " also replace slash / on the main layer.

The numbers row is further optimized so '10' is easily and quickly typed as middle-index roll.


### beakl-15-matrix

![beakl-15-matrix](/images/kb-beakl-15-matrix.png)

BEAKL 15 was the previous recommended layout. It improves on BEAKL 9 vowels and punctuations.

BEAKL 15 combines the consonant half of BEAKL 9 and the vowel half of BEAKL 10, reducing the reaching and overuse of the left index finger.

The numbers row is again rearranged where 5 is centered on right hand.

Numbers also moved from the symbols layer to the <a href="#numpad">numpad</a> layer. Thus numbers can be accessed at the numbers row and the tenkey-like numpad.


### beakl-10-matrix

![beakl-10-matrix](/images/kb-beakl-10-matrix.png)

BEAKL 10 by moving the R to the home middle finger, this supposedly enables great rolls on the right hand consonant district.


### beakl-9-matrix

![beakl-9-matrix](/images/kb-beakl-9-matrix.png)

BEAKL 9 was an erstwhile recommended layout. Outstanding performance for both English and non-English prose and code.

The I is returned to the left home ring finger and H to the top. Like BEAKL EZ, it's excellent in English and other many other languages.


### beakl-8-matrix

![beakl-8-matrix](/images/kb-beakl-8-matrix.png)

BEAKL 8, the I is moved to bottom row index, reducing effort on ring finger. Ideally excellent on English, but falters a bit on non-English; however, it can be hard to get used to if your left hand is not nimble enough.


### beakl-7-matrix

![beakl-7-matrix](/images/kb-beakl-7-matrix.png)

Starting with BEAKL 7, the H and vowels stay within the home block, focusing on the home and top rows, and lessening the effort by the left pinky to the minimum. The consonant district solidifies to near optimal. The MP/CF pairs are interchangeable to either top or bottom row.


### p_rn

![p_rn](/images/kb-p_rn.png)

P_RN puts three letters on the thumb cluster, further improving scores across the board: better distance, finger usage, and same finger and same hand scores. However, this requires a special keyboard that allows this setup. Such keyboards are extremely hard to find; yet should improve the typing experience by a great deal.


### beakl-5-matrix

![beakl-5-matrix](/images/kb-beakl-5-matrix.png)

BEAKL 5 targets efficient coding by placing the parentheses unshifted on the bottom row; thus displacing J and X to the outside.


### beakl-ez-matrix

![beakl-ez-matrix](/images/kb-beakl-ez-matrix.png)

BEAKL EZ is optimized for both English and Chinese Pinyin (Zhongwen); hence EZ moniker. It also coincidentally performs slightly better on other languages than other BEAKL layouts. The - and ; are left unshifted to accomodate certain (Pinyin) IMEs that utilize them.

It is recommended to create a layer for a left hand <a href="#numpad">numpad</a> (not shown). The number row symbols are there to match the (older) numpad layer for easier memorization.


### beakl-stretch

![beakl-stretch](/images/kb-beakl-stretch.png)

BEAKL Stretch experiments with the home row all the way at the bottom of the board. This is inspired by the fact that fingers are more adept at stretching than curling. Thus all keys are accessed by stretching, never curling.

The numbers row is also optimized. The zero 0 is moved to left hand, so that the most common numbers can be typed while the right hand remains on the mouse. The numbers are ordered in recognizable sequence, per hand.


### beakl-opted4-ergo-alt

![beakl-opted4-ergo-alt](/images/kb-beakl-opted4-ergo-alt.png)

BEAKL 4 from here forward, the <a href="#punctuation">symbols</a> and <a href="#numpad">numbers</a> are accessed by the Alt-Gr layer. Learning from Arensito, this ingenious move makes typing numbers and puncs easy and fast, without reaching all over the keyboard, thus staying in the home area, and without need to look down on the keyboard. The effect is much better scores, lower effort for source code and numeric entries.

The number row is left empty for the sake of KLA analysis. In practice you can leave the numbers there to give extra keys to games that use many shortcuts.


### beakl-opted3

![beakl-opted3](/images/kb-beakl-opted3.png)

BEAKL Opted3 the K now at pinky home, H moved to ring top row. Thus pinky usage reduced even further. L moved to better spot at middle top. Number row split by odd/even; still favoring 0 and 1.


### beakl-opted1

![beakl-opted1](/images/kb-beakl-opted1.png)

BEAKL Opted1 first version created by the Opt program. The particular vowel cluster is optimal and is repeatedly found in the same arrangement in subsequent layouts. H and P are likely candidates for pinky home. Q and X are placed outside the main 30-key block, replaced by more common punctuations. Numbers stay on same side as standard, but optimized around 0 and 1.


## Additional Layers

### Punctuation

![Punctuation](/images/kb-beakl-puncs.png)

Punctuations are placed on the same keys as the letters so the fingers do not have to reach far away. Thus the typist doesn't have to look down at the keyboard to find the right punctuation, the fingers stay on the home area, and they are typed at much faster rates.

The punc layer can be accessed with Alt-Gr or other modifier or state-lock key (e.g. unused Scroll Lock can be turned into a layer access key.)

### Numpad

![Left-hand Numpad](/images/kb-beakl-numpad-full.png)

Access the numpad layer with the Num-Lock key using the left pinky.

The tenkey can be typed with the left hand, while the right hand stays on the mouse. The numbers are optimized for frequency and distance, just like the letters on the main layer.

The right hand can access the letters used in hexadecimal values.

Most symbols are also available. Some are accessed with Shift modifier while staying on the numpad layer.

 
## Archived Layouts

Earlier layouts created manually that predated and inspired BEAKL. (In reverse chronological order.)

### balanced v

![balanced v](/images/kb-balancedv.png)

Balanced V imitates the Dvorak style of placing all the vowels on single hand, thus raising hand alternation.

### amuseum

![amuseum](/images/kb-amuseum.png)

Amuseum attempts to balance both hands by splitting the vowels between hands. Number row is copied by Dvorak's original vision. Pinkies are somewhat less common letters compared to layouts by other people to slightly reduce pinky usage.

## Learn More

* Learn about the [history of BEAKL](history.php)
* Understand the [BEAKL principles](beakl.php)
	</pre>
</div>

</body>
</html>
